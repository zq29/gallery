#Similar Document Search

Oct 12, 2018, Advanced Database System final project, Sun Yat-sen University

---

Upload a text document and then you get the **most similar ones among millions**!

How to speed it up? How to save memories?

## How It Works

Background knowledge:

* **TF-IDF**: [https://en.wikipedia.org/wiki/Tf%E2%80%93idf](https://en.wikipedia.org/wiki/Tf–idf)
* **Non-negative matrix factorization**: [https://en.wikipedia.org/wiki/Non-negative_matrix_factorization](https://en.wikipedia.org/wiki/Non-negative_matrix_factorization)
* **Locality-sensitive hashing**: [https://en.wikipedia.org/wiki/Locality-sensitive_hashing](https://en.wikipedia.org/wiki/Locality-sensitive_hashing)

Now, let's work through this process following the data flow:

###Extract Text Features and Load Them into Memories

First, we need a good representation for documents, 

---

**I'm working on translating this readme from Chinese to English...**

---

<br><br><br><br><br><br>

假设有100万个纯文本数据，使用有2万英文单词的字典，并用TF-IDF矩阵表示，为了工程上的简单，需要将这个矩阵放进内存里，下面来计算一下这个矩阵的大小：

```
使用2万大小的词典，向量化文档后矩阵大小为：
1M(文本数)*20K(字典大小)*4B(一个浮点数)=80GB

为了节省空间，假设平均每篇文章有1000个不同的单词，使用矩阵的稀疏表示(使用<行，列，元素值>)，不考虑坐标开销的情况下：
1M(文本数)*1K(词向量TF-IDF)*4B(一个浮点数)=4GB
```

上面的`4GB`是理论值，实际上，由于词向量的维度常在1.5K左右，Python中一个浮点数占用了24B的内存，文本数有112万，这样，起码就需要`40GB`的内存，而我的笔记本电脑只有`8GB`内存，所以需要更小维度的数据。

* **文本采样**

在实际操作中，将99万的维基百科文本数据做如下采样：

从文本中找出100-200词的段落组成新的文本集，伪代码如下（概率参数为实际应用中较优值）：

```python
# 对应<wiki_data.py>文件中的<_sample_and_split_docs>函数

结果 = []
for doc in 99万本文:
    将 doc 按段落划分
    以7%的概率选中某段 p：
    	while p 小于100词：
        	p = p 与下一段的拼接结果
            将下一段从循环队列中删除
        if 100 < p 的词数 < 200:
            将 p 加入 结果
return 结果
```

运行上述采样算法后得到了`112万`段100-200词的纯文本。这样词向量基本控制在了100维左右，8GB内存就可以装下TF-IDF稀疏矩阵了。

* **TF-IDF**

使用最基本的TF-IDF定义计算：

<img src="imgs/tf.png" height='50px'>

<img src="imgs/idf.png" height="50px">

### 数据表示与降维

为了将数据进行降维进行更快的运算同时减少对内存的使用，需要对矩阵进行分解。

首先尝试了使用SVD分解，但由于分解后的矩阵中有负值出现，不符合接下来Weighted Minhash的要求。

* **使用非负矩阵分解(NMF)**

<img src="imgs/nmf.png">

见参考[3]，分解得到的矩阵W和H有很好的性质，即矩阵元素均非负。实际操作中，原矩阵V的维度是(1,120,000, 25,322)，分解后W和H的维度分别是(1,120,000, 64)，(64, 25,322)。

分解后的W矩阵即可看做原TF-IDF矩阵的一个表示。

### 局部敏感哈希(LSH)

* **Weighted Minhash**

由于向量均为实数，这里使用了Weighted Minhash，可以认为是Minhash的实数版本，更多关于Weighted Minhash的介绍见下文参考\[4\]\[6\]。

* **Minhash LSH**

然后使用了Minhash LSH来做索引与搜索，与课上讲的一致。

##项目框架与实现

###项目框架

```mermaid
graph LR

a((" "))
b[数据采集]
c[数据预处理]
d[LSH]
e[查询]
a---b
a---c
a---d
a---e

b1[Wiki数据文本提取]
b2[文本采样]
b---b1
b---b2

c1[计算TD-IDF稀疏矩阵]
c2[NMF分解]
c3[计算伪逆矩阵]
c---c1
c---c2
c---c3

d1[WeightedMinHash]
d2[MinHashLSH]
d---d1
d---d2

e1[快速计算]
e2[查询服务器]
e---e1
e---e2
```

### 工程实现

#### Wikipedia数据集

维基百科开放了自己的数据供用户下载，见参考[1]，同时还有已经实现好的文本提取工具，见参考[2]。

本项目中使用了99万左右的维基百科词条，经过提取和采样后，共112万条文本，平均写入了112个文件。

#### 文本采样

见上文。

####计算TD-IDF稀疏矩阵

```mermaid
graph LR
a[加载字典]
b[向量化文本]
c[计算TF-IDF]
a-->b
b-->c
```

常见英文单词字典中有25322个词，见参考[8]，英文分词较简单，工程上需要注意的是将大小写保持一致，并且注意`he's`这种情况需要分词。

向量化文本后需要计算TD-IDF矩阵，按照公式计算即可，需要注意的是需要存下25322维的IDF向量供以后计算使用。

#### 非负矩阵分解(NMF)

这里使用了sklearn库中的矩阵分解，实际实现中发现该方法远远慢于SVD分解，并且分解效果不是特别令人满意，即分解得到的W、H矩阵后得到的矩阵和原矩阵还是有一定的差距，测试代码如下：

```python
def test_NMF():
	mat = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
	model = NMF(n_components = 3, init = 'nndsvdar')
	W = model.fit_transform(mat)
	H = model.components_
	X = np.matmul(W, H)
	print(np.allclose(mat, X))
    
# 注：allclose函数比较两个numpy数组是否相同(足够接近)
# 输出结果：False
```

由于该误差，在接下来新来的文本计算完TF-IDF值并且降维以后，相同的文本在经过矩阵分解和部分解的情况下，会得到不同的值。

#### Weighted MinHash

这里使用了已经实现好的Minhash，见参考[5]。

####MinHashLSH

核心代码如下：

```python
mg = WeightedMinHashGenerator(dim = 64, sample_size = 64)
lsh = MinHashLSH(threshold = 0.7, num_perm = 64)
for i, e in enumerate(W):
	# prevent all zero exception thrown by minhash()
	ezeros = (e == 0)
	if ezeros.all(): e[0] = 1e-5
	lsh.insert(i, mg.minhash(e))
```

即将W矩阵的每一行（代表一个文档）做完hash之后通过`insert`函数插入到`lsh`对象中去。

#### 使用伪逆快速计算

当TF-IDF矩阵来了新的一行（新的文本）进行查询时，为了避免再次进行矩阵分解，进行如下数学推导：

$$
X_{TF-IDF}=WH\\
Let\ \vec{x}\ be\ a\ row\ of\ X_{TF-IDF},\vec{w}\ be\ the\ corresponding\ row\ of\ W,then\\
\vec{x}=\vec{w}H\\
(\vec{x})^T=(\vec{w}H)^T\\
H^T\vec{w}^T=\vec{x}^T\\
Let\ G\ be\ the\ pseudoinverse\ of\ H^T,with\ property\ H^TG\vec{v}=\vec{v},then\\
H^TG\vec{x}^T=\vec{x}^T\\
i.e.\ \vec{w}^T=G\vec{x}^T
$$

由上述证明可知，当TF-IDF矩阵增加新的一行时，使用G矩阵($H^T$矩阵的伪逆)左乘该行即可得到W矩阵新的一行(的转置)。关于矩阵伪逆以及使用代码进行计算，见参考\[10\][11]。

在代码实现中，只需要将G矩阵计算并保存即可，做完LSH之后就可以丢弃W、H矩阵，查询的核心代码如下：

```python
def query(doc_text):
	sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(doc_text, 					IDF_COUNT_VEC)
	wt = np.matmul(MATRIX_G, sparse_tfidf.reshape((sparse_tfidf.shape[0], 1)))
	results = LSH.query(MG.minhash(wt.T.flatten()))
	return results
```

#### 查询服务器

总结一下在做完上述处理之后，查询时需要加载到内存中的量：

* 112万文档的IDF向量：计算新文本的TF-IDF值时使用
* G矩阵：即NMF分解得到的H矩阵的伪逆，可以快速得到降维后的新的W的一行，见上述数学推导
* WeightedMinHashGenerator对象：用来做Hash的对象，由于有随机初始化，所以需要保存下来，见参考[5]
* MinHashLSH：用来索引和查询Hash值的做局部敏感哈希(LSH)的对象，见参考[5]
* 常用英文单词字典：用来向量化文本，见参考[8]

以上对象在硬盘中共占用`839MB`空间，实际载入内存后需要`4GB`以上的空间，在启动服务器时将其载入内存，当查询请求到来时即可快速响应。

使用简单的前端网页来进行查询的交互：

<img src="imgs/q.png" width="1000px">

<img src="imgs/q_result.png" width="1000px">

### 正确性验证及性能

为了验证正确性与测量查询的性能，使用如下代码进行测试：

```python
# <query.py> 中的 <mock_query> 函数
	...
	print('Start querying...')
	t1 = time.time()
	for i in range(count):
		sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(
			docs_texts[i], IDF_COUNT_VEC)
		wt = np.matmul(MATRIX_G, sparse_tfidf.reshape((sparse_tfidf.shape[0], 1)))
		print(LSH.query(MG.minhash(wt.T.flatten())))

	t2 = time.time()
	print('Average query speed: %ss/query' % ((t2 - t1) / count))
    ...
```

做100次查询，打印输出结果截图如下：

<img src="imgs/test1.png">

100次查询分别使用了id为0-99的文档作为新文档进行查询，从红圈圈出的结果可以看出，每个文档的相似文档中都有自己。得到的结果基本分散在区间[0, 112万]之间，

需要注意的是，由于NMF分解产生的误差，新文档在作为查询文档时计算出的新的向量与分解得到W矩阵中的对影向量不是完全相同的，是高度相似的，即$\vec{w}^T\approx G\vec{x}^T$，可以看出，LSH的表现还不错，这个结果基本可以说明结果是正确的。

<img src="imgs/test2.png">

对于性能，100次查询的平均用时大概在0.02秒/次。除了使用LSH做查询，后面还有在查询结果中使用更精确的相似度衡量方法、服务器响应的网络延迟等时间，不过这些的开销都很少了。

以上程序运行在配置如下的电脑上（未使用GPU）：

```
CPU: Intel i5-6300HQ @ 2.3GHz
RAM: 8GB
系统：Windows 10
```

实际测试中，以查询id为`1040975`的文本为例，查询结果如下：

```

Query time: 0.013965368270874023

IO time: 0.15126323699951172

Similar docs are as follows:

Content: [0946664] qiagen introduced the first kit for purification of plasmids   small ring shaped dna molecules in bacterial cells   in       cutting the preparation time for plasmids from between two and three days down to two hours  the company has a global market share exceeding     for certain sample preparation technologies multinational pharmaceutical companies use molecular sample and assay technologies in all phases of the drug development process  from the fundamental pharmaceutical research stage  through preclinical and clinical studies  to the commercialization and application of new products qiagen technologies can be used to identify genes that participate in the emergence of diseases  carry out studies on the functions and interactions between genes  or proteins  in entire biological pathways  identify and validate potential biomarkers and identify and evaluate therapeutic target molecules and suitable active agent candidates  qiagen s products can also be used in the execution of the clinical studies 


Content: [1040975]      by year end in the european union        mwp had been installed  mainly in spain  gemasolar  in spain  was the first to provide    hour power description  solar heating is the usage of solar energy to provide space or water heating       at present the eu is second after china in the installations      if all eu countries used solar thermal as enthusiastically as the austrians  the eu s installed capacity would already be    gwth      million m today  far beyond the target of     million m by       set by the white paper in            the research efforts and infrastructure needed to supply     of the energy for space and water heating and cooling across europe using solar thermal energy was set out under the aegis of the european solar thermal technology platform  esttp   published in late december       more than     experts developed the strategic research agenda  sra   which includes a deployment roadmap showing the non technological framework conditions that will enable this ambitious goal to be reached by      
```

可以看到运行查询算法的时间在0.014s左右，按索引找出原文的IO时间在0.15s左右。

得到的结果有id为`1040975`的自身文档，也与另外一个文档，两个文档的均为`technology`和`research`等相关的。

## 心得体会

* 找数据集

  找数据集的过程花了很多时间，是比较麻烦的一个环节，尝试了一些网站的评论（文本太短）以及爬虫抓取的海量网页（从不同网站的html提取纯文本比较麻烦、网站内容参差不齐包含各种语言）等。最后用了Wikipedia公开的数据库，然后自己做了采样处理。

* 矩阵分解

  在用了SVD做降维以后发现用Weighted MinHash时负值是不大好处理的，所以改用了NMF分解，修改这一部分也花了不少时间，包括需要学习一些伪逆的知识以及进行简单的数学推导。

* 数据量

  在思路清晰以后，代码的难度并不大，各种库都有写好的工具。但是在数据量如此大的时候，每次去测试都需要十分谨慎，做NMF分解大概需要半小时左右，做LSH的insert也需要半小时左右，其中若是出现一些小差错就会需要重新再跑一遍程序，比较耗时。

* 学到了很多知识

  不仅学到了TF-IDF、SVD、NMF、LSH这些方法，还在实际写代码时发现了一些处理大数据时的小技巧，比如将处理结果分成大约8MB大小并做索引，就能极大减少IO的开销；用一些工具如pickle将处理中间结果存储在硬盘，就可以让很长的数据处理流程按小阶段进行，降低编程难度等。

## 使用说明

### 介绍

从112万100-200词的文档中寻找相似文档，查找时间在0.02s左右，硬盘IO时间在0.2-0.5s左右。

### 部署及可用时间说明

本项目部署在实验室的个人电脑上，通过请求转发映射到公网IP，各种情况会导致个人电脑的服务下线（如：Win10自动更新、网线被踢断、被其他人不小心关掉等，以上问题均发生过），**因此下文中提供的网站服务是不可靠的，随时可能下线，如不可用请稍后再试**，两周内我会尽量保证服务的可用性（**10月27日到11月10日**），如过期可联系我（qianzch@qq.com）随时可以将服务再次上线。

```
个人电脑配置：
Intel Core i7-6700 CPU @ 3.40GHz 3.41GHz
RAM: 8.00G
OS: Windows 10 64bit 教育版
```

###使用步骤

1. 访问网址：http://123.207.11.16:8080/page/

   会看到如下页面：

   <img src="imgs/u1.png">

2. 复制网页中的一段推荐的文档或任意文档(0-2000词之间)到搜索框中，如下图所示

   * **请输入至少一个常见的英文单词，否则(如输入`123`时)将会看到报错界面**

   <img src="imgs/u2.png">

3. 点击`Query`按钮，即可得到搜索结果，如下图所示：

   <img src="imgs/u3.png">

   * 会显示`Query time`，单位为秒，是执行查找的时间

     * 通常查询时间在0.02秒左右

   * 还会显示`IO time`，单位为秒，是从文件中索引出源文档

     * **实际运行时，第二次执行相同查询IO时间会极大下降**

       这是因为缓存的原因，通过工具`RamMap`可以发现，查询的文档所在的文件在第一次查询后就会被载入内存

     * 文件在内存时，通常IO时间在0.2-0.5秒左右

## 参考

* [1]维基百科数据库：https://dumps.wikimedia.org/enwiki/latest/
* [2]维基百科文本提取器：http://medialab.di.unipi.it/wiki/Wikipedia_Extractor
* [3]非负矩阵分解：https://www.cnblogs.com/pinard/p/6812011.html?utm_source=itdadao&utm_medium=referral
* [4]实数向量如何做Hash：https://cs.stackexchange.com/questions/44997/finding-similar-high-dimensional-real-vectors
* [5]局部敏感哈希(LSH)的实现：https://github.com/ekzhu/datasketch
* [6]Weighted Minhash：http://static.googleusercontent.com/media/research.google.com/en//pubs/archive/36928.pdf
* [7]Minhash LSH：http://infolab.stanford.edu/~ullman/mmds/ch3.pdf
* [8]常用英文单词字典：https://github.com/dolph/dictionary
* [9]sklearn NMF分解：http://scikit-learn.org/stable/modules/generated/sklearn.decomposition.NMF.html
* [10]Moore–Penrose inverse：https://en.wikipedia.org/wiki/Moore%E2%80%93Penrose_inverse
* [11]Numpy pinv：https://docs.scipy.org/doc/numpy-1.15.1/reference/generated/numpy.linalg.pinv.html
