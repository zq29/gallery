#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-17

import time
import pickle
import numpy as np
from datasketch import WeightedMinHashGenerator
from datasketch import MinHashLSH

import wiki_data

# for SVD, currently use NMF
def load_u_s_vt():
	with open('./storage/matrix_u', 'rb') as f:
		u = pickle.load(f)
	with open('./storage/matrix_s', 'rb') as f:
		s = pickle.load(f)
	with open('./storage/matrix_vt', 'rb') as f:
		vt = pickle.load(f)
	return u, s, vt

# for NMF
def load_W_H():
	with open('./storage/matrix_W', 'rb') as f:
		W = pickle.load(f)
	with open('./storage/matrix_H', 'rb') as f:
		H = pickle.load(f)
	return W, H

# the pseudoinverse of H
def cal_and_save_G():
	_, H = load_W_H()
	G = np.linalg.pinv(H.T)
	with open('./storage/matrix_G', 'wb') as f:
		pickle.dump(G, f)

def load_G():
	with open('./storage/matrix_G', 'rb') as f:
		G = pickle.load(f)
	return G

def do_lsh():
	t1 = time.time()

	print('Loading W and H matrix...')
	W, H = load_W_H()
	t2 = time.time()
	print('Done in %ss!' % (t2 - t1))

	print('Doing minhash...')
	mg = WeightedMinHashGenerator(dim = 64, sample_size = 64)
	lsh = MinHashLSH(threshold = 0.7, num_perm = 64)
	for i, e in enumerate(W):
		# prevent all zero exception thrown by minhash()
		ezeros = (e == 0)
		if ezeros.all(): e[0] = 1e-5
		lsh.insert(i, mg.minhash(e))
	t3 = time.time()
	print('Done in %ss!' % (t3 - t2))

	print('Writing lsh obj to file...')
	with open('./storage/mg_obj', 'wb') as f:
		pickle.dump(mg, f)
	with open('./storage/lsh_obj', 'wb') as f:
		pickle.dump(lsh, f)
	t4 = time.time()
	print('Done in %ss!' % (t4 - t3))

	print('Testing query for 30 times...')
	for i in range(30):
		print(lsh.query(mg.minhash(W[i])))
	t5 = time.time()
	print('Average query speed: %ss/query' % ((t5 - t4) / 30))

def load_mg():
	with open('./storage/mg_obj', 'rb') as f:
		mg = pickle.load(f)
	return mg

def load_lsh():
	with open('./storage/lsh_obj', 'rb') as f:
		lsh = pickle.load(f)
	return lsh


if __name__ == '__main__':
	
	do_lsh()
	cal_and_save_G()


	'''
	u, s, vt = load_u_s_vt()
	mat = np.matmul(u, np.diag(s))
	'''
	'''
	v1 = np.random.uniform(-10, 10, 10)
	v2 = np.random.uniform(-10, 10, 10)
	v3 = np.random.uniform(-10, 10, 10)
	mg = WeightedMinHashGenerator(10, 5)
	m1 = mg.minhash(v1)
	m2 = mg.minhash(v2)
	m3 = mg.minhash(v3)

	# Create weighted MinHash LSH index
	lsh = MinHashLSH(threshold=0.1, num_perm=5)
	lsh.insert("m2", m2)
	lsh.insert("m3", m3)
	result = lsh.query(m1)
	print("Approximate neighbours with weighted Jaccard similarity > 0.1", result)
	'''