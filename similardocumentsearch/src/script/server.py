#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-20

print('Starting server, please wait...')
import web
import time
import query # this may cost seconds
import wiki_data
print('Server started!')

urls = (
	'/', 'index'
)

def handle_query(qdoc_text):
	query_time = time.time()
	results = query.query(qdoc_text)
	query_time = time.time() - query_time

	if len(results) == 0:
		return 'No similar docs found!\nQuery time: %ss' % query_time

	reslut_docs = []
	other_time = time.time()
	for i in results:
		doc = wiki_data.get_doc_by_id(i)
		if doc is None:
			doc = 'Invalid doc ID!'
		reslut_docs.append(doc)
	other_time = time.time() - other_time

	template = 'Query time: {}\n\nIO time: {}\n\nSimilar docs are as follows:\n\n{}'
	doc_template = 'Content: {}\n\n'
	return template.format(
			query_time, other_time,
			''.join([doc_template.format(doc) for doc in reslut_docs])
		)

class index:
	def GET(self):
		return "Please use <POST> method!"
	
	def POST(self):
		args = web.input()
		try:
			doc_text = args.doc_text
		except AttributeError as e:
			return str(e)
		if len(doc_text) == 0 or doc_text.isspace():
			return 'Doc must not be an empty string!'
		if len(doc_text) > 2000:
			return 'Doc length > 2000, abort!'

		return handle_query(doc_text)
		

if __name__ == "__main__":
	app = web.application(urls, globals())
	app.run()