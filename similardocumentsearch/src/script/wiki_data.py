#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-11

import os, gc, time, random
from bs4 import BeautifulSoup

# ----------------------------------------- origin wiki data ------------------------------------------

# dir 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 has total 999,209 docs
DATA_DIRS 	= ['../wiki_dump/text' + str(e) for e in range(1, 13)]

TEST_DIR 	= '../wiki_dump/text1'
TEST_FILE 	= '../wiki_dump/text1/AA/wiki_05'

def _docs_file_parser(filename):
	bs = BeautifulSoup(open(filename, encoding = 'utf-8'), 'html5lib')
	docs = bs.findAll('doc')
	# get attr by docs[0]['id'] & docs[0].text
	return docs

def _count_docs_from_dir(dir_path, show = True):
	count = 0
	for root, dirs, files in os.walk(dir_path, topdown = False):
		for name in files:
			filename = os.path.join(root, name)
			docs = _docs_file_parser(filename)
			count += len(docs)
			if show: print('Done with file %s' % filename)
	return count

def _get_docs_from_file(filename):
	docs = _docs_file_parser(filename)
	data = []
	for doc in docs:
		data.append((doc['id'], doc.text))
	return data

def _count_words(text_str):
	return len(text_str.split())

def get_docs_from_dir(dir_path, show = True):
	data = []
	for root, dirs, files in os.walk(dir_path, topdown = False):
		for name in files:
			filename = os.path.join(root, name)
			docs = _docs_file_parser(filename)
			for doc in docs:
				try:
					data.append((doc['id'], doc.text))
				except AttributeError as e:
					print(str(e))
			if show: print('Done with file %s' % filename)
	return data

def get_test_docs():
	return _get_docs_from_file(TEST_FILE)
	

# ----------------------------------------- process(ed) wiki data ------------------------------------------
PROC_DATA_PREFIX = '../data/'

PROC_TEST_FILE 	= '../data/result_0.txt'

def _sample_and_split_docs(
		docs, 
		docs_sample_rate,
		indoc_sample_rate,
		min_word_count,
		max_word_count
	):
	sampled_paragraphs = []
	for doc in docs:
		# sample docs
		if random.random() > docs_sample_rate: continue
		# indoc sample
		paragraphs = doc[1].split('\n')
		while paragraphs:
			if random.random() > indoc_sample_rate:
				paragraphs.pop(0)
				continue
			temp_para = paragraphs.pop(0)
			# contenate paragrahp if too short
			while paragraphs and _count_words(temp_para) < min_word_count:
				temp_para += paragraphs.pop(0)
			if min_word_count < _count_words(temp_para) < max_word_count:
				sampled_paragraphs.append(temp_para)
	return sampled_paragraphs

def _format_and_write_results(results, start_id, filename = './results.txt'):
	with open(filename, 'w', encoding = 'utf-8') as f:
		cur_id = start_id
		for e in results:
			words = '[{:07d}] '.format(cur_id)
			cur_id += 1
			words += ''.join(x if x.isalpha() else ' ' for x in e)
			words = words.lower()
			f.write(words + '\n')
	return cur_id

def _processed_text_parser(filename):
	with open(filename, 'r', encoding = 'utf-8') as f:
		contents = f.readlines()
	docs = []
	for e in contents:
		doc_id = int(e[1: 8])
		docs.append((doc_id, e))
	return docs

def process_data(docs_per_file = 10000):
	results = []
	cur_result_file_id = 0
	cur_result_doc_id = 0
	
	for cur_dir in DATA_DIRS:
		
		docs = get_docs_from_dir(cur_dir)
		results += _sample_and_split_docs(docs, 1, 0.07, 100, 200)

		print('Total original docs: %s' % len(docs))
		print('Current result length: %s' % len(results))
		
		del docs
		gc.collect()

		while len(results) > docs_per_file:
			filename = '{}result_{}.txt'.format(
				PROC_DATA_PREFIX, str(cur_result_file_id))
			cur_result_file_id += 1
			cur_result_doc_id = _format_and_write_results(
				results[0: docs_per_file], cur_result_doc_id, filename)
			results = results[docs_per_file + 1: ]

def get_processed_test_docs():
	return _processed_text_parser(PROC_TEST_FILE)

def get_all_processed_docs():
	filepaths = ['{}result_{}.txt'.format(PROC_DATA_PREFIX, e) for e in range(112)]
	docs = []
	for file in filepaths:
		docs += _processed_text_parser(file)
	return docs

def get_doc_by_id(doc_id):
	start_time = time.time()
	file_id = int(doc_id / 10000)
	line_id = doc_id % 10000
	doc_path = '{}result_{}.txt'.format(PROC_DATA_PREFIX, file_id)
	try:
		with open(doc_path, encoding = 'utf-8') as f:
			for i, line in enumerate(f):
				if i == line_id:
					return line
	except Exception as e:
		print(str(e))
	return None

if __name__ == '__main__':
	'''
	count = 0
	for dir_path in [TEST_DIR]:
		count += _count_docs_from_dir(dir_path, False)
		print('Done with dir %s, total count %s' % (dir_path, count))
	'''
	#process_data()
	#print(get_doc_by_id(23589))

	reslut_docs = []
	other_time = time.time()
	for i in range(10):
		doc = get_doc_by_id(random.randint(0, 1120000 - 1))
		if doc is None:
			doc = 'Invalid doc ID!'
		reslut_docs.append(doc)
	other_time = time.time() - other_time
	print('Average query time: %s' % other_time)