#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-12

import gc, sys, time, random
import pickle
import numpy as np
from scipy.sparse import csr_matrix
from scipy.sparse.linalg import svds
from sklearn.decomposition import NMF

import wiki_data

def _load_dictionary():
	with open('./storage/dictionary.txt') as f:
		dictionary_list = f.read().split()
	dictionary_dict = dict(zip(dictionary_list, [i for i in range(len(dictionary_list))]))
	return dictionary_list, dictionary_dict

dictionary_list, dictionary_dict = _load_dictionary()


# [[(0, 1), (500, 10)], # no.0 doc, with the 0th word 1 & 500th word 10
# 	[(10, 3), (100, 4), (20000, 1)]]
def _vectorize_doc(doc):
	data = []
	col_ind = []
	words = doc[1].split()
	for word in words:
		if word in dictionary_dict:
			dict_pos = dictionary_dict[word]
			if dict_pos in col_ind:
				data[col_ind.index(dict_pos)] += 1
			else:
				col_ind.append(dict_pos)
				data.append(1)
	return data, col_ind

# invoke this is prohibited since it changes file <'idf_count_vec'>
def _cal_tfidf_data(data, row_ind, col_ind, docs_count):
	# calculate idf list
	idf = np.ones(len(dictionary_list), dtype = float)
	for e in col_ind:
		idf[e] += 1.0
	with open('./storage/idf_count_vec', 'wb') as f:
		pickle.dump(idf, f)
	# prevent divided by 0 warning
	idf = np.array([x - 1 if x > 1.5 else 1 for x in idf], dtype = float)
	idf = np.log(docs_count / idf)
	# calculate tf in every doc
	last_row_ind_ptr = 0
	row_ind_ptr = 0
	last_row = row_ind[0]
	while row_ind_ptr < len(row_ind):
		# count entries in the same row
		while row_ind_ptr < len(row_ind)\
			and row_ind[row_ind_ptr] == last_row:
			row_ind_ptr += 1
		if row_ind_ptr < len(row_ind):
			last_row = row_ind[row_ind_ptr]
		# process one row
		word_count = 0
		for i in range(last_row_ind_ptr, row_ind_ptr):
			word_count += data[i]
		for i in range(last_row_ind_ptr, row_ind_ptr):
			data[i] /= word_count
		last_row_ind_ptr = row_ind_ptr
	# multiply idf
	for i in range(len(data)):
		data[i] *= idf[col_ind[i]]
	return data

# invoke this is prohibited since it changes file <'idf_count_vec'>
def _get_tfidf_sparse_matrix(docs):
	data = []
	row_ind = []
	col_ind = []
	for i, doc in enumerate(docs):
		data_ele, col_ind_ele = _vectorize_doc(doc)
		data += data_ele
		col_ind += col_ind_ele
		row_ind += [i for _ in range(len(col_ind_ele))]
	data = _cal_tfidf_data(data, row_ind, col_ind, len(docs))
	return csr_matrix(
			(data, (row_ind, col_ind)),
			shape = (len(docs), len(dictionary_list)),
			dtype = float
		)

def load_idf_count_vec():
	with open('./storage/idf_count_vec', 'rb') as f:
		idf_count = pickle.load(f)
	return idf_count

# magic number, set manually by countting total docs
def get_sparse_tfidf_vec_with_idf_count(doc_text, idf_count, total_docs = 112e4):
	# tf
	words = doc_text.split()
	# word_count = len(words) this wrong since some words may not be in the dict
	tf_count = np.zeros(len(dictionary_list))
	for word in words:
		if word in dictionary_dict:
			tf_count[dictionary_dict[word]] += 1
	word_count = np.sum(tf_count)
	tf = tf_count / word_count

	'''
	# v1: recalculating idf
	# recalculating idf is necessary(?) since small change of the denominator
	# brings big change in value when denominator is small
	idf_count = np.copy(idf_count)
	for i in range(len(tf)):
		if tf[i] > 0:
			idf_count[i] += 1
	# prevent divided by 0 warning
	idf_count = np.array([x - 1 if x > 1.5 else 1 for x in idf_count], dtype = float)
	idf = np.log(total_docs / idf_count)
	'''
	# v2: use old idf
	idf_count = np.copy(idf_count)
	idf_count = np.array([x - 1 if x > 1.5 else 1 for x in idf_count], dtype = float)
	idf = np.log(total_docs / idf_count)

	return tf * idf

def do_preprocess():
	t1 = time.time()

	print('Reading all docs from hard drive...')
	#docs = wiki_data.get_processed_test_docs()[: 1000]
	docs = wiki_data.get_all_processed_docs()
	t2 = time.time()
	print('Done in %ss!' % (t2 - t1))

	print('Constructing sparse matrix...')
	mat = _get_tfidf_sparse_matrix(docs)
	del docs
	gc.collect()
	t3 = time.time()
	print('Done in %ss!' % (t3 - t2))

	'''
	print('Doing SVD...')
	u, s, vt = svds(mat, k = 64, which = 'LM') # LM: largest singular value
	del mat
	gc.collect()
	t4 = time.time()
	print('Done in %ss!' % (t4 - t3))
	'''
	print('Doing NMF...')
	model = NMF(n_components = 64, init = 'nndsvdar')
	W = model.fit_transform(mat)
	H = model.components_
	del mat
	gc.collect()
	t4 = time.time()
	print('Done in %ss!' % (t4 - t3))

	'''
	print('Writing SVD results to files...')
	with open('matrix_u', 'wb') as f:
		pickle.dump(u, f)
	with open('matrix_s', 'wb') as f:
		pickle.dump(s, f)
	with open('matrix_vt', 'wb') as f:
		pickle.dump(vt, f)
	t5 = time.time()
	print('Done in %ss!' % (t5 - t4))
	'''
	print('Writing NMF results to files...')
	with open('./storage/matrix_W', 'wb') as f:
		pickle.dump(W, f)
	with open('./storage/matrix_H', 'wb') as f:
		pickle.dump(H, f)
	t5 = time.time()
	print('Done in %ss!' % (t5 - t4))

if __name__ == '__main__':
	do_preprocess()

	'''
	data = [2, 3, 4, 5, 6, 7, 8, 9]
	row_ind = [0, 0, 1, 1, 1, 2, 2, 3]
	col_ind = [1, 2, 0, 1, 2, 0, 1, 2]
	#data = _cal_tfidf_data(data, row_ind, col_ind, 4)
	#print(data)

	mat = csr_matrix((data, (row_ind, col_ind)),
			shape = (4, 3),
			dtype = float
		)

	print(mat)
	'''