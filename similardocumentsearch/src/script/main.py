#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-20

import wiki_data
import pre_proc
import lsh
import query

if __name__ == '__main__':
	# WARNING: you should NEVER run this script
	# if you do not clearly know what you're doing
	wiki_data.process_data()
	pre_proc.do_preprocess()
	lsh.do_lsh()
	lsh.cal_and_save_G()
	query.mock_query()