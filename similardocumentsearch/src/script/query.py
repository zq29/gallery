#!/usr/bin/env python
# -*- coding: utf-8 -*-

# author: qianzch@qq.com
# 2018-10-17

import time, pickle, random
import numpy as np
from datasketch import WeightedMinHashGenerator
from datasketch import MinHashLSH

import wiki_data
import pre_proc
import lsh

IDF_COUNT_VEC 	= pre_proc.load_idf_count_vec()
MATRIX_G 		= lsh.load_G()
MG 				= lsh.load_mg()
LSH 			= lsh.load_lsh()

def generate_fake_doc_text():
	dict_len = len(pre_proc.dictionary_list)
	
	# generate a vector <v> so that len(v) = dict_len & sum(v) = 1
	v = np.zeros(dict_len)
	# v[np.random.randint(0, dict_len, 100)] += 0.01
	QUOTA = 200
	prob = 1
	while prob > 0:
		use_p = random.randint(1, 10) * (1 / QUOTA) # [0.005, 0.05]
		prob -= use_p
		v[random.randint(0, dict_len - 1)] += use_p
	# around 150 words
	v = np.array([int(e * 150 + 0.5) for e in v], dtype = int) # [0.75, 7.5]
	words = []
	for i, e in enumerate(v):
		if e > 0:
			words += [pre_proc.dictionary_list[i]
				for _ in range(e)]
	random.shuffle(words)
	doc = ' '.join(words)
	return doc


def mock_query(count):
	print('Loading docs..')
	docs = wiki_data.get_processed_test_docs()
	docs_texts = [e[1] for e in docs]
	if count > len(docs):
		raise ValueError('Param <count> should be less than %s!' % len(docs))

	print('Start querying...')
	t1 = time.time()
	for i in range(count):
		sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(
			docs_texts[i], IDF_COUNT_VEC)
		wt = np.matmul(MATRIX_G, sparse_tfidf.reshape((sparse_tfidf.shape[0], 1)))
		print(LSH.query(MG.minhash(wt.T.flatten())))

	t2 = time.time()
	print('Average query speed: %ss/query' % ((t2 - t1) / count))

def jaccard_similarity(list1, list2):
	s1 = set(list1)
	s2 = set(list2)
	return len(s1.intersection(s2)) / len(s1.union(s2))

def query(doc_text):
	sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(doc_text, IDF_COUNT_VEC)
	wt = np.matmul(MATRIX_G, sparse_tfidf.reshape((sparse_tfidf.shape[0], 1)))
	results = LSH.query(MG.minhash(wt.T.flatten()))
	return results

if __name__ == '__main__':
	mock_query(100)
	'''
	docs = wiki_data.get_processed_test_docs()

	t = time.time()
	for _ in range(100):
		doc_text = random.choice(docs)[1]
		results = query(doc_text)
		#print(doc_text)
		for e in results:
			print('Jaccard Similarity: %s' % jaccard_similarity(doc_text, wiki_data.get_doc_by_id(e)))
	print('Time used:%s' % (time.time() - t))
	'''