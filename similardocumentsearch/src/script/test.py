import sys
import numpy as np
from datasketch import WeightedMinHashGenerator
from datasketch import MinHashLSH
from sklearn.decomposition import NMF

import wiki_data
import pre_proc
import lsh

def compare_1d_np_vecs(v1, v2):
	if v1.shape != v2.shape:
		print('v1 and v2 are of different shape!')
		return False
	if len(v1.shape) != 1:
		print('Vecs must be of 1 dim!')
		return False
	v1 = np.sort(v1)
	v2 = np.sort(v2)
	if np.allclose(v1, v2):
		print('v1 and v2 are the SAME!')
		return True
	else:
		ix = v1.shape[0]
		for i in range(32):
			print('%s\t\t%s' % (v1[ix - i - 1], v2[ix - i - 1]))
		print('v1 and v2 are different!')
	return False

'''Test two TF-IDF algo
data = [1, 3, 4, 5, 1]
row_ind = [0, 0, 1, 1, 2]
col_ind = [1, 2, 0, 1, 0]
docs_count = 3
v1 = pre_proc._cal_tfidf_data(data, row_ind, col_ind, docs_count)
IDF_COUNT_VEC = pre_proc.load_idf_count_vec()
doc = 'aardvark aargh aargh aargh'
v2 = pre_proc.get_sparse_tfidf_vec_with_idf_count(doc, IDF_COUNT_VEC, 3)
docs = [(0, 'aardvark aargh aargh aargh'),
		(1, 'aa aa aa aa aardvark aardvark aardvark aardvark aardvark'),
		(2, 'aa')]
mat = pre_proc._get_tfidf_sparse_matrix(docs).toarray()
print(compare_1d_np_vecs(v2, mat[0]))
'''

'''
docs = wiki_data.get_processed_test_docs()[: 1000]
tfidf_mat = pre_proc._get_tfidf_sparse_matrix(docs)
IDF_COUNT_VEC = pre_proc.load_idf_count_vec()
tfidf_vec = pre_proc.get_sparse_tfidf_vec_with_idf_count(docs[0][1], IDF_COUNT_VEC, 1000)
compare_1d_np_vecs(tfidf_mat.toarray()[0], tfidf_vec)
'''

''' Test LSH library
v1 = np.arange(10)
v2 = np.arange(10)
v3 = np.arange(10)
mg = WeightedMinHashGenerator(10, 5)
m1 = mg.minhash(v1)
m2 = mg.minhash(v2)
m3 = mg.minhash(v3)

# Create weighted MinHash LSH index
lsh = MinHashLSH(threshold=0.99999, num_perm=5)
lsh.insert("m2", m2)
lsh.insert("m3", m3)
result = lsh.query(m1)
print("Approximate neighbours with weighted Jaccard similarity", result)
'''
''' Some Test
for i in range(count):
	sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(docs_texts[i], 112e4)
	wt = np.matmul(MATRIX_G, sparse_tfidf.reshape((1, sparse_tfidf.shape[0])).T)
	print(wt.T.flatten())
	#print(LSH.query(MG.minhash(wt.T.flatten())))
'''

def test_NMF():
	mat = np.array([[1, 2, 3], [4, 5, 6], [7, 8, 9]])
	#mat = np.random.random(100).reshape((4, 25))
	model = NMF(n_components = 3, init = 'nndsvdar')
	W = model.fit_transform(mat)
	H = model.components_
	print(mat)
	X = np.matmul(W, H)
	print(X)
	print(np.allclose(mat, X))

if __name__ == '__main__':
	IDF_COUNT_VEC = pre_proc.load_idf_count_vec()
	test_docs = wiki_data.get_processed_test_docs()[: 1000]
	doc_text = test_docs[0][1]
	#doc_text = generate_fake_doc_text()
	sparse_tfidf = pre_proc.get_sparse_tfidf_vec_with_idf_count(doc_text, IDF_COUNT_VEC, 1000)
	#W, H = lsh.load_W_H()
	#TFIDF = np.matmul(W, H)
	mat = pre_proc._get_tfidf_sparse_matrix(test_docs)
	model = NMF(n_components = 64, init = 'nndsvdar')
	W = model.fit_transform(mat)
	H = model.components_
	TFIDF = np.matmul(W, H)
	print(np.allclose(mat.toarray(), TFIDF))

	# TODO:WRONG TF-IDF
	#Temp = np.matmul(H.T, MATRIX_G)
	xt1 = sparse_tfidf.flatten()
	xt2 = TFIDF[0].flatten()

	compare_1d_np_vecs(xt1, xt2)
	#print(np.allclose(xt1, xt2))
	#print(np.allclose(np.matmul(Temp, xt), xt))
	
	#print(xt)
	#wt = np.matmul(MATRIX_G, xt)
	#print(wt.T)

	#mock_query(100)