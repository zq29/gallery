# 项目源代码说明

###文件组织

提交后的项目文件组织及说明如下：

```
|-- data 						# 存放采样过后的Wikipedia文本数据
|     |-- result_0.txt
|
|-- script 						# 源代码以及持久化存储的对象
|     |-- storage 				# 持久化的对象
|          |-- dictionary.txt	# 词典
|          |-- idf_count_vec*	# 计算idf用到的向量
|          |-- lsh_obj*			# lsh对象
|          |-- matrix_G*		# G矩阵(见项目报告，下同)
|          |-- matrix_H*		# H矩阵
|          |-- matrix_W*		# W矩阵
|          |-- mg_obj*			# 做Weighted MinHash的对象
|     |-- index.html			# 查询前端
|     |-- lsh.py				# LSH模块
|     |-- main.py				# 其中的代码展示了整个程序的逻辑，不建议运行
|     |-- pre_proc.py			# 数据预处理（矩阵分解等）
|     |-- query.py				# 查询模块
|     |-- server.py				# 查询服务器
|     |-- test.py				# 简单的测试代码
|     |-- wiki_data.py			# 进行原数据的采样等
|
|-- wiki_dump					# Wikipedia公开的数据集
|     |-- enwiki-latest-pages
|		-articles1.xml-p10p30302.bz2*
|     |-- extractor.py			# 提取文本数据工具
```

说明：

* `data`文件夹中的数据只提供了一个文件展示用，可以用`wiki_dump`文件夹中的`extractor.py`得到全部数据，示例代码`python extractor.py enwiki-latest-pages-articles1.xml-p10p30302.bz2`
* `wiki_dump`文件夹中的数据只提供了一个文件展示用，其余可以从Wikipedia公开的数据库得到，见项目报告参考[1]

* 标有`*`标志的文件因为过大，所以用空文件代替，仅做展示

### 环境配置

```
Windows 10
Python 3.6
```

### 复现实验流程

* 下载Wikipedia数据，放在指定文件夹

* 使用extractor提取文本，得到的结果组织方式见上文

* 按顺序执行如下程序

  ```
  python wiki_data.py
  python pre_proc.py
  python lsh.py
  ```

  此时执行`python query.py`即可测试查询性能

* 开启服务器

  `python server.py 80`

* 访问网页

  访问`http://localhost`，即可使用