# Processor

Nov 05 2019, an Duke ECE 550 project

<img src="CPU.png">

This is a CPU I implemented with Verilog, check `src` for source code.

## Quartus IP tools

### Generating I-Mem

* RAM: 1-Port
  * 32 bit output
  * 12 bit address, 4096 words (specified in skeleton)
* Delay: from directly reading from I-Mem - less than 30ns (typically 27ns)

### Generating D-Mem

* Identical as I-Mem

## Regfile

* Delay: less than 10ns (typically 7ns)
* cannot read & write the same reg
* Destination can only be **\$rd**

## Control Signal

See `control signal.xlsx`

### Op Decoder

* Delay: less than 10ns (typically 8.4ns), but I guess it's fine

## ALU

* Op ADD delay: less than 15ns (typically 14.5 ns)







## Test Instructions

```asm
# the first instruction begin with address 8
#8
addi $1, $0, 1 #$1: 1
addi $2, $0, 2 #$2: 2
add $3, $1, $2 #$3: 3
add $4, $0, $3 #$4: 3
#12
sub $5, $2, $1 #$5: 1
and $6, $1, $2 #$6: 0
or $7, $1, $2 #$7: 3
sll $8, $3, 2 #$8: 12
#16
sra $9, $8, 2 #$9: 3
sw $8, 1($1) #save $8 to D-Mem[1 + 1]
lw $10, 1($1) #$10: 12, load from D-Mem[1 + 1] to $10
add $11, $10, $0 #$11: 12, verify sw and lw
#20
j 22 #jump to PC = 16, which is "sra $9, $8, 2"
nop
bne $3, $4, 32 #$3 == $4 == 3, no jump
bne $1, $2, 1 #$1 != $2, jump to PC + 1 + 1
#24
nop
jal 27 #$31: 26, jump to 27
nop
addi $12, $31, 4 #$12: 30, verify jal
#28
jr $12 #jump from 28 to 30
nop
blt $2, $1, 32 #not gonna jump
blt $1, $2, 1 #$1 < $2, jump to PC + 1 + 1
#32
nop
bex 1 #not gonna happen
sll $13, $1, 30 #prepare to overflow
add $14, $13, $13 #overflow
#36
bex 38 #jump to "setx 0"
nop
setx 0
```

Binary code:

```
#8
00101 00001 00000 00000000000000001
00101 00010 00000 00000000000000010
00000 00011 00001 00010 00000 00000 00
00000 00100 00000 00011 00000 00000 00
#12
00000 00101 00010 00001 00000 00001 00
00000 00110 00001 00010 00000 00010 00
00000 00111 00001 00010 00000 00011 00
00000 01000 00011 00000 00010 00100 00
#16
00000 01001 01000 00000 00010 00101 00
00111 01000 00001 00000000000000001
01000 01010 00001 00000000000000001
00000 01011 01010 00000 00000 00000 00
#20
00001 000000000000000000000010110
00000000000000000000000000000000
00010 00011 00100 00000000000100000
00010 00001 00010 00000000000000001
#24
00000000000000000000000000000000
00011 000000000000000000000011011
00000000000000000000000000000000
00101 01100 11111 00000000000000100
#28
00100 01100 0000000000000000000000
00000000000000000000000000000000
00110 00010 00001 00000000000100000
00110 00001 00010 00000000000000001
#32
00000000000000000000000000000000
10110 000000000000000000000000001
00000 01101 00001 00000 11110 00100 00
00000 01110 01101 01101 00000 00000 00
#36
10110 000000000000000000000100110
00000000000000000000000000000000
10101 000000000000000000000000000
```





* How do you determine a branch should occur for bne?

  By using the output "isNotEqual" from ALU.

  I implemented the control module using random logic (Slide 08 P31), and there's a signal called "op_bne" which indicates if the instruction is "bne". If "op_bne" and "isNotEqual" are true at the same time, then the processor should use the PC specified by bne instruction.

* How do you determine a branch should occur for blt?

  Same as bne, I used "op_blt" to check if it's a blt instruction and used "isLessThan" output from ALU to see if we actually need to branch.

* What is the slowest instruction in your processor? Explain why you know this.

  The slowest is "lw". It first get the instruction address from PC, and then read from IMem for the instruction, and read from RegFile for address, and use ALU to calculate address with offset, and then read from DMem and finally write back to RegFile. It basically needs every previous component's result before going to the next. Other instructions, like add, may not even need to wait for the DMem before write back.

* Estimate your processor's maximum clock speed in ns (i.e. the fastest clock speed that allows the slowest instruction to correctly execute).

  12.5MHz. Since 40ns period won't work and it works with 80ns period.

* Prove the above clock estimation by attaching a waveform of your processor functioning at this speed.

* What clocking scheme does your processor follow, i.e., how is each clocked element in your design clocked? 

  |             | Posedge | Negedge |
  | ----------- | ------- | ------- |
  | PC Register | X       |         |
  | Regfile     | X       |         |
  | Dmem        |         | X       |
  | Imem        |         | X       |

* Why did you choose the above clocking scheme?

  Let the 12.5MHz clock be "clk", and the 25MHz clock be "clk2", 50MHz clock be "clk4" (when positive edge of "clk" comes, "clk2" and "clk4" also have a positive edge) below explained how I designed the clocknig scheme.

  PC is triggered by the positive edge of "clk". And after 10ns, which is the first negative edge of "clk4", the processor read from IMem. And then there are 50ns before it actually reaches DMem, that is, DMem is triggered by the second negative edge of "clk2". Then 20ns after DMem, the next positive edge of "clk" arrives, that's when data is written back to RegFile and it goes to the next PC.

  

  And below are my reasons why I designed such a scheme.

  First, it is obvious that PC should be triggered by the clock with the longest period, and I chose positive edge (not much difference in logic whether positive or negative edge).

  Then,

  (a) PC and IMem can NOT be triggered at the same time, since the current PC value would be unstable if we do this.

  (b) The delay between PC and IMem is really short so I want to trigger IMem as soon as PC is ready, so IMem is trigger at the first negative edge of "clk4"

  (c) The time slot from IMem is triggered to DMem is triggered takes most of the time (read from IMem, wait for control logic, read from Regfile, and ALU), so I leave it with the longest time I can (50ns).

  (d) It takes time for data reading from DMem gets ready, so DMem is triggered before write back happens; and along with (c), it explains why DMem is triggered at the second negative edge of "clk2"

  (e) The previous instruction's write back can actually happen at the next instruction's cycle, and it won't affect anything since the data is already written before the next read. So I use the positive edge of "clk" to trigger RegFile (since only write operation of RegFile need a clock)

	Used 3 clocks with different frequency to help me with clocking scheme. IMem and DMem are triggered by negative edge using clocks with higher frequency. Regfile is trigger with the same posedge as PC

* What module(s) do you use to compute the next PC?

  32 bit adder

* Regarding the question above, why did you choose these modules for computing the next PC?

  If using the existing ALU, the input of ALU get a new source (PC), then we need a mux and a control signal 

  Else if using a new ALU, it is a waste of area on the chip

  So a 32 bit adder works great for this purpose

  ```
  32 bit adder to get PC+1, ALU to determine if branch happens, where it should take PC from sources other than PC+1
  ```

* How did you prepare your processor design? Ex. You created an initial visual model through Logisim or by hand, you broke down the processor into modules you wanted to design, etc.

  (a) I created 3 versions of visual models by hand, and countless small modifications on them

  (b) I created the design following the same kind of steps as the Slide 08, P10-28. I pick one instruction at a time, and add the necessary components to the previous design to make the new instruction work

  (c) I used Excel to create the truth table for the control module

  (d) I draw the clocks by hand to verify if all instructions work on this clocking scheme

* Which of the following testing methods did you use throughout your design process? (Select all that apply)

  * Waveform tests of single modules
  * Personally created testbenches for single modules
  * Waveform tests of subgroups of modules
  * Personally created testbenches for subgroups of modules
  * Waveform tests of cumulative processor design
  * Personally created testbenches for cumulative processor design
  * Independent web research and resolution of error/warning messages
  * Submitting to AG350 and receiving feedback on failed test cases

* Describe one time when you found a bug in your processor: how did you find, isolate, and resolve it?

  Clocking scheme: the processor works well in RTL simulation. But when I run tests with same instructions under Gate-level simulation with real delay, all outputs were wrong. The problem is quite obvious: logics are write, timing is wrong. So I divided the 25MHz clock I used to 12.5MHz, and leave most of the time of one cycle to what happens between IMem and DMem (as described in previous questions), and then it works.
  
  Actually I did not run into any significant bugs since I was doing increasemental testing, but countless small bugs. And what took most of my time (60% at least) in this checkpoint is designing the clocking scheme and verify it. Since I was not familiar with the generated IMem and DMem and what the actual delays are for each component. 
  
  ```
  One fixed bug: processor worked well in RTL simulation, but not under Gate-level simulation with dela. The problem is quite obvious: logics are right, timing is wrong. So I divided the 25MHz clock to 12.5MHz, and leave most of the time of one cycle to what happens between IMem and DMem
  ```
  
  

