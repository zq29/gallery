module op_decoder(
	op_add, 
	op_addi, 
	op_sub, 
	op_and, 
	op_or, 
	op_sll, 
	op_sra, 
	op_sw, 
	op_lw, 
	op_j, 
	op_bne, 
	op_jal, 
	op_jr, 
	op_blt, 
	op_bex, 
	op_setx,
	opcode,
	aluop);
	
	output op_add;
	output op_addi;
	output op_sub;
	output op_and;
	output op_or;
	output op_sll;
	output op_sra;
	output op_sw;
	output op_lw;
	output op_j;
	output op_bne;
	output op_jal;
	output op_jr;
	output op_blt;
	output op_bex;
	output op_setx;
	input[4: 0] opcode;
	input[4: 0] aluop;
	
	wire is_Rtype;
	assign is_Rtype = opcode ? 0 : 1;
	
	wire[4: 0] n_opcode, n_aluop;
	not_5_bit n1(n_opcode, opcode);
	not_5_bit n2(n_aluop, aluop);
	
	assign op_add = is_Rtype && (n_aluop[2] & n_aluop[1] & n_aluop[0]);
	
	assign op_addi = n_opcode[4] & n_opcode[3] & opcode[2] & n_opcode[1] & opcode[0];
	
	assign op_sub = is_Rtype && (n_aluop[2] & n_aluop[1] & aluop[0]);
	assign op_and = is_Rtype && (n_aluop[2] & aluop[1] & n_aluop[0]);
	assign op_or = is_Rtype && (n_aluop[2] & aluop[1] & aluop[0]);
	assign op_sll = is_Rtype && (aluop[2] & n_aluop[1] & n_aluop[0]);
	assign op_sra = is_Rtype && (aluop[2] & n_aluop[1] & aluop[0]);
		
	assign op_sw = n_opcode[4] & n_opcode[3] & opcode[2] & opcode[1] & opcode[0];
	assign op_lw = n_opcode[4] & opcode[3] & n_opcode[2] & n_opcode[1] & n_opcode[0];
	assign op_j = n_opcode[4] & n_opcode[3] & n_opcode[2] & n_opcode[1] & opcode[0];
	assign op_bne = n_opcode[4] & n_opcode[3] & n_opcode[2] & opcode[1] & n_opcode[0];
	assign op_jal = n_opcode[4] & n_opcode[3] & n_opcode[2] & opcode[1] & opcode[0];
	assign op_jr = n_opcode[4] & n_opcode[3] & opcode[2] & n_opcode[1] & n_opcode[0];
	assign op_blt = n_opcode[4] & n_opcode[3] & opcode[2] & opcode[1] & n_opcode[0];
	assign op_bex = opcode[4] & n_opcode[3] & opcode[2] & opcode[1] & n_opcode[0];
	assign op_setx = opcode[4] & n_opcode[3] & opcode[2] & n_opcode[1] & opcode[0];


	
endmodule

module not_5_bit(n_out, n_in);
	output[4: 0] n_out;
	input[4: 0] n_in;
	
	not n4(n_out[4], n_in[4]);
	not n3(n_out[3], n_in[3]);
	not n2(n_out[2], n_in[2]);
	not n1(n_out[1], n_in[1]);
	not n0(n_out[0], n_in[0]);
endmodule

