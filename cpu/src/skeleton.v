/**
 * NOTE: you should not need to change this file! This file will be swapped out for a grading
 * "skeleton" for testing. We will also remove your imem and dmem file.
 *
 * NOTE: skeleton should be your top-level module!
 *
 * This skeleton file serves as a wrapper around the processor to provide certain control signals
 * and interfaces to memory elements. This structure allows for easier testing, as it is easier to
 * inspect which signals the processor tries to assert when.
 */

module skeleton(clock, reset, imem_clock, dmem_clock, processor_clock, regfile_clock
	/*
	// my add
	// Imem
	,address_imem, q_imem,
	// RegFile
	my_Rwe, ctrl_writeReg, ctrl_readRegA, ctrl_readRegB, data_writeReg, data_readRegA, data_readRegB,
	// Dmem
	address_dmem, data, wren, q_dmem,
	// PC
	pc
	*/
	);
    input clock, reset;
    output imem_clock, dmem_clock, processor_clock, regfile_clock;
	 /*
	 // my add
	 // Imem
	 output [11:0] address_imem;
    output [31:0] q_imem;
	 // Regfile
	 output my_Rwe;
	 output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
    output [31:0] data_writeReg;
    output [31:0] data_readRegA, data_readRegB;
	 // Dmem
	 output [11:0] address_dmem;
    output [31:0] data;
    output wren;
    output [31:0] q_dmem;
	 // PC
	 output [31: 0] pc;
	 */
	 
	 // Clocks
	 //wire imem_count;
	 //dffe_1_bit imem_counter(.q(imem_count), .d(~imem_count), .clk(~imem_clock), .en(1'b1), .clr(reset));
	 
	 wire clock2, clock4;
	 clock_divider_2 freq2(clock, reset, clock2); // T: 40ns
	 clock_divider_2 freq4(clock2, reset, clock4); // T: 80ns 
	 assign imem_clock = ~clock & clock2 & clock4;
	 assign dmem_clock = clock & ~clock2 & ~clock4;
	 assign processor_clock = clock4; // clock for PC
	 assign regfile_clock = clock4; // write at pos edge
	

    /** IMEM **/
    // Figure out how to generate a Quartus syncram component and commit the generated verilog file.
    // Make sure you configure it correctly!
    wire [11:0] address_imem;
    wire [31:0] q_imem;
    imem my_imem(
        .address    (address_imem),            // address of data
        .clock      (/*imem_count*/imem_clock),                  // you may need to invert the clock
        .q          (q_imem)                   // the raw instruction
    );

    /** DMEM **/
    // Figure out how to generate a Quartus syncram component and commit the generated verilog file.
    // Make sure you configure it correctly!
    wire [11:0] address_dmem;
    wire [31:0] data;
    wire wren;
    wire [31:0] q_dmem;
    dmem my_dmem(
        .address    (address_dmem),       // address of data
        .clock      (dmem_clock),                  // may need to invert the clock
        .data	    (data),    // data you want to write
        .wren	    (wren),      // write enable
        .q          (q_dmem)    // data from dmem
    );

    /** REGFILE **/
    // Instantiate your regfile
	 wire my_Rwe;
    wire ctrl_writeEnable;
	 assign my_Rwe = ((~clock2 & clock4) | (clock2 & ~clock4)) ? 1'b0 : ctrl_writeEnable;
    wire [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
    wire [31:0] data_writeReg;
    wire [31:0] data_readRegA, data_readRegB;
    regfile my_regfile(
        regfile_clock,
        my_Rwe/*ctrl_writeEnable*/,
        reset,
        ctrl_writeReg,
        ctrl_readRegA,
        ctrl_readRegB,
        data_writeReg,
        data_readRegA,
        data_readRegB
    );

    /** PROCESSOR **/
    processor my_processor(
        // Control signals
        processor_clock,                          // I: The master clock
        reset,                          // I: A reset signal

        // Imem
        address_imem,                   // O: The address of the data to get from imem
        q_imem,                         // I: The data from imem

        // Dmem
        address_dmem,                   // O: The address of the data to get or put from/to dmem
        data,                           // O: The data to write to dmem
        wren,                           // O: Write enable for dmem
        q_dmem,                         // I: The data from dmem

        // Regfile
        ctrl_writeEnable,               // O: Write enable for regfile
        ctrl_writeReg,                  // O: Register to write to in regfile
        ctrl_readRegA,                  // O: Register to read from port A of regfile
        ctrl_readRegB,                  // O: Register to read from port B of regfile
        data_writeReg,                  // O: Data to write to for regfile
        data_readRegA,                  // I: Data from port A of regfile
        data_readRegB,                   // I: Data from port B of regfile
		  pc
    );

endmodule
