`timescale 1 ns / 100 ps

module processor_tb();
	reg clock, reset;
	 /*
	 // Imem
	 wire [11:0] address_imem;
    wire [31:0] q_imem;
	 // Regfile
	 wire my_Rwe;
	 wire [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
    wire [31:0] data_writeReg;
    wire [31:0] data_readRegA, data_readRegB;
	 // Dmem
	 wire [11:0] address_dmem;
    wire [31:0] data;
    wire wren;
    wire [31:0] q_dmem;
	 // PC
	 wire[31: 0] pc;
	 */
	wire imem_clock, dmem_clock, processor_clock, regfile_clock;
	
	
	skeleton my_skeleton(.clock(clock), .reset(reset), 
		.imem_clock(imem_clock), .dmem_clock(dmem_clock), .processor_clock(processor_clock), .regfile_clock(regfile_clock)
		/*
		// my add
		// Imem
		,.address_imem(address_imem), .q_imem(q_imem),
		// RegFile
		.my_Rwe(my_Rwe), .ctrl_writeReg(ctrl_writeReg), .ctrl_readRegA(ctrl_readRegA), .ctrl_readRegB(ctrl_readRegB),
		.data_writeReg(data_writeReg), .data_readRegA(data_readRegA), .data_readRegB(data_readRegB),
		.address_dmem(address_dmem), .data(data), .wren(wren), .q_dmem(q_dmem),
		.pc(pc)
		*/
	);
	
	// decalre variables here
	//integer i, j, k;
	/*
	wire[31: 0] data_result;
	reg[31: 0] a, b;
	alu my_alu(
		.data_operandA(a), 
		.data_operandB(b), 
		.ctrl_ALUopcode(0),
		.data_result(data_result)
		//.isNotEqual,
		//.isLessThan,
		//.overflow
	);
	*/
	
	/*
	// need to set imem as the top-level entry
	reg[11:0] address_imem;
	wire[31:0] q_imem;
	imem my_imem(
        .address    (address_imem),            // address of data
        .clock      (~clock),                  // you may need to invert the clock
        .q          (q_imem)                   // the raw instruction
    );
	 */
	 
	 /*
	// need to set regfile as the top-level entry
	reg ctrl_reset;
	reg ctrl_writeEnable;
    reg [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
    reg [31:0] data_writeReg;
    wire [31:0] data_readRegA, data_readRegB;
    regfile my_regfile(
        clock,
        ctrl_writeEnable,
        ctrl_reset,
        ctrl_writeReg,
        ctrl_readRegA,
        ctrl_readRegB,
        data_writeReg,
        data_readRegA,
        data_readRegB
    );
	 */
	
	
	
	
	
	
	
	
	
	
	initial begin
		clock = 0;
		reset = 1;
		@(negedge clock)
		reset = 0;
		# 3000
		$stop;
	end

	// Clock generator
	always
		#10	clock = ~clock;
	
	/*
	task testImem;
		//input [4:0] writeReg;
		@(posedge clock)
		@(posedge clock)
		@(posedge clock)
		@(posedge clock)
		@(posedge clock)
		begin
			for(i = 0; i < 80; i = i + 1) begin
				@(posedge clock)
				address_imem = address_imem + 1;
			end
		end
	endtask
	*/
	
	/*
	task testRegfile; 
	begin
		ctrl_reset = 1;
		# 10
		ctrl_reset = 0;
		# 20
		
		@(negedge clock)
		ctrl_writeEnable <= 1;
		ctrl_writeReg <= 1;
		ctrl_readRegA <= 1;
		ctrl_readRegB <= 0;
		data_writeReg <= 255;
		
		# 50
		@(negedge clock)
		ctrl_writeEnable <= 0;
		ctrl_writeReg <= 0;
		ctrl_readRegA <= 0;
		ctrl_readRegB <= 1;
		data_writeReg <= 0;
		# 100
		
		ctrl_writeEnable <= 1;
		for(i = 1; i < 32; i = i + 1) begin
			@(negedge clock)
			ctrl_writeReg <= i;
			data_writeReg <= i;
		end
		
		# 100
		ctrl_writeEnable <= 0;
		for(i = 1; i < 32; i = i + 1) begin
			@(negedge clock)
			ctrl_readRegA <= i;
			ctrl_readRegB <= i;
		end
	end
	endtask
	*/

endmodule
