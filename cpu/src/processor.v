/**
 * READ THIS DESCRIPTION!
 *
 * The processor takes in several inputs from a skeleton file.
 *
 * Inputs
 * clock: this is the clock for your processor at 50 MHz
 * reset: we should be able to assert a reset to start your pc from 0 (sync or
 * async is fine)
 *
 * Imem: input data from imem
 * Dmem: input data from dmem
 * Regfile: input data from regfile
 *
 * Outputs
 * Imem: output control signals to interface with imem
 * Dmem: output control signals and data to interface with dmem
 * Regfile: output control signals and data to interface with regfile
 *
 * Notes
 *
 * Ultimately, your processor will be tested by subsituting a master skeleton, imem, dmem, so the
 * testbench can see which controls signal you active when. Therefore, there needs to be a way to
 * "inject" imem, dmem, and regfile interfaces from some external controller module. The skeleton
 * file acts as a small wrapper around your processor for this purpose.
 *
 * You will need to figure out how to instantiate two memory elements, called
 * "syncram," in Quartus: one for imem and one for dmem. Each should take in a
 * 12-bit address and allow for storing a 32-bit value at each address. Each
 * should have a single clock.
 *
 * Each memory element should have a corresponding .mif file that initializes
 * the memory element to certain value on start up. These should be named
 * imem.mif and dmem.mif respectively.
 *
 * Importantly, these .mif files should be placed at the top level, i.e. there
 * should be an imem.mif and a dmem.mif at the same level as process.v. You
 * should figure out how to point your generated imem.v and dmem.v files at
 * these MIF files.
 *
 * imem
 * Inputs:  12-bit address, 1-bit clock enable, and a clock
 * Outputs: 32-bit instruction
 *
 * dmem
 * Inputs:  12-bit address, 1-bit clock, 32-bit data, 1-bit write enable
 * Outputs: 32-bit data at the given address
 *
 */
module processor(
	// Control signals
	clock,						  // I: The master clock
	reset,						  // I: A reset signal

	// Imem
	address_imem,				   // O: The address of the data to get from imem
	q_imem,						 // I: The data from imem

	// Dmem
	address_dmem,				   // O: The address of the data to get or put from/to dmem
	data,						   // O: The data to write to dmem
	wren,						   // O: Write enable for dmem
	q_dmem,						 // I: The data from dmem

	// Regfile
	ctrl_writeEnable,			   // O: Write enable for regfile
	ctrl_writeReg,				  // O: Register to write to in regfile
	ctrl_readRegA,				  // O: Register to read from port A of regfile
	ctrl_readRegB,				  // O: Register to read from port B of regfile
	data_writeReg,				  // O: Data to write to for regfile
	data_readRegA,				  // I: Data from port A of regfile
	data_readRegB,				   // I: Data from port B of regfile
	
	// my add
	pc
);
	// Control signals
	input clock, reset;

	// Imem
	output [11:0] address_imem;
	input [31:0] q_imem;

	// Dmem
	output [11:0] address_dmem;
	output [31:0] data;
	output wren;
	input [31:0] q_dmem;

	// Regfile
	output ctrl_writeEnable;
	output [4:0] ctrl_writeReg, ctrl_readRegA, ctrl_readRegB;
	output [31:0] data_writeReg;
	input [31:0] data_readRegA, data_readRegB;

	// my add
	output[31: 0] pc;
	
	// ----------------------------------- MY INPLEMENTATION ----------------------------------------
	
	
	// PC
	wire[31: 0] pc, pc_plus_1, pc_in, pc_sources_1, pc_sources_2; // index by word
	
	// I-Mem
	wire[4: 0] opcode;
	wire[4: 0] rd_sources_1, rd_sources_2, rs_sources_1, rt_sources_1, rt_sources_2;
	wire[4: 0] rd, rs, rt;
	wire[4: 0] shamt;
	wire[4: 0] aluop;
	wire[16: 0] immediate;
	wire[26: 0] target;
	// sign extend
	wire[31: 0] immediate_32_bit;
	// op decode
	wire op_add, op_addi, op_sub, op_and, op_or, op_sll, op_sra;
	wire op_sw, op_lw, op_j, op_bne, op_jal, op_jr, op_blt, op_bex, op_setx;
	
	// Reg File
	wire ctrl_reset;
	wire[31: 0] data_writeReg_sources_1, data_writeReg_sources_2, data_writeReg_sources_3;
	
	// ALU
	wire[31: 0] data_operandA, data_operandB;
	wire[5: 0] ctrl_ALUopcode;
	wire[4: 0] ctrl_shiftamt;
	wire[31: 0] data_result;
	wire isNotEqual;
	wire isLessThan;
	wire overflow;
	
	// Control Signals
	wire Rdst, JP, ALUinB;
	
	
	
	// PC
	dffe_32_bit my_pc(
		.q(pc), 
		.d(pc_in), 
		.clk(clock), 
		.en(1'b1), 
		.clr(reset)
	);
	adder_32_bit pc_plus_1_adder(
		.sum(pc_plus_1), 
		.a(pc), 
		.b(32'h00000001), 
		.is_sub_op(0)
	);

	
	
	// I-Mem, 27ns
	assign address_imem = pc[11: 0];
	/*
	imem my_imem(
		.address(address_imem),
		.clock(~clock),
		.q(q_imem)
	);
	*/
	assign opcode = q_imem[31: 27];
	assign rd_sources_1 = q_imem[26: 22];
	assign rs_sources_1 = q_imem[21: 17];
	assign rt_sources_1 = q_imem[16: 12];
	assign shamt = q_imem[11: 7];
	assign aluop = q_imem[6: 2];
	assign immediate = q_imem[16: 0];
	assign target = q_imem[26: 0];
	// sign extend, ignore the delay
	immediate_sx sx(immediate_32_bit, immediate);
	// op decode, 8.4ns
	op_decoder my_op_decoder(
		.op_add(op_add), 
		.op_addi(op_addi), 
		.op_sub(op_sub), 
		.op_and(op_and), 
		.op_or(op_or), 
		.op_sll(op_sll), 
		.op_sra(op_sra), 
		.op_sw(op_sw), 
		.op_lw(op_lw), 
		.op_j(op_j), 
		.op_bne(op_bne), 
		.op_jal(op_jal), 
		.op_jr(op_jr), 
		.op_blt(op_blt), 
		.op_bex(op_bex), 
		.op_setx(op_setx),
		.opcode(opcode),
		.aluop(aluop)
	);
	
	
	
	
	// Reg File, 7ns
	// assign ctrl_writeEnable; // see below control signal
	assign ctrl_reset = reset;
	assign ctrl_writeReg = rd;
	assign ctrl_readRegA = rs;
	assign ctrl_readRegB = rt;
	//data_readRegA,
	//data_readRegB
	/*
	regfile my_regfile(
		clock, 
		ctrl_writeEnable, 
		ctrl_reset, 
		ctrl_writeReg,
		ctrl_readRegA, 
		ctrl_readRegB, 
		data_writeReg, 
		data_readRegA,
		data_readRegB
	);
	*/
	// Below is what's under RegFile in the design picture
	assign rd_sources_2 = op_jal ? 5'b11111 : 5'b11110; // $30($rstatus), $31($ra)
	assign rd = (overflow | op_setx | op_jal) ? rd_sources_2 : rd_sources_1;
	assign rs = op_bex ? 5'b11110 : rs_sources_1;
	assign rt_sources_2 = op_bex ? 5'b00000 : rt_sources_1;
	assign rt = Rdst ? rd : rt_sources_2;
	// Below is selecting which data need to write to RegFile
	assign data_writeReg = op_setx ? target : data_writeReg_sources_3;
	assign data_writeReg_sources_1 = op_lw ? q_dmem : data_result;
	wire rstatus_sources_1, rstatus_sources_2;
	assign rstatus_sources_1 = op_addi ? 32'h00000002 : 32'h00000003;
	assign rstatus_sources_2 = op_add ? 32'h00000001 : rstatus_sources_1;
	assign data_writeReg_sources_2 = overflow ? rstatus_sources_2 : data_writeReg_sources_1;
	assign data_writeReg_sources_3 = op_jal ? pc_plus_1 : data_writeReg_sources_2;
	
	
	
	
	
	// ALU, 15ns
	assign data_operandA = data_readRegA;
	assign data_operandB = ALUinB ? immediate_32_bit : data_readRegB;
	assign ctrl_ALUopcode = (op_addi | op_sw | op_lw) ? 5'b00000 : aluop; // non-R type but still need ALU
	assign ctrl_shiftamt = shamt;
	alu my_alu(
		data_operandA, 
		data_operandB, 
		ctrl_ALUopcode,
		ctrl_shiftamt,
		data_result,
		isNotEqual,
		isLessThan,
		overflow
	);
	
	
	
	// D Mem
	assign address_dmem = data_result;
	assign data = data_readRegB;
	assign wren = op_sw;
	
	
	
	// Below is what on top of the design picture, mainly branches and jumps
	wire[31: 0] branch_adder_sum;
	wire bne_and_out, blt_and_out, branch_or_out, pc_mux_helper;
	adder_32_bit branch_adder(.sum(branch_adder_sum), .a(pc_plus_1), .b(immediate_32_bit), .is_sub_op(1'b0));
	assign bne_and_out = op_bne & isNotEqual;
	assign blt_and_out = op_blt & (~isLessThan) & isNotEqual;
	assign branch_or_out = bne_and_out | blt_and_out;
	assign pc_sources_1 = branch_or_out ? branch_adder_sum : pc_plus_1;
	assign pc_mux_helper = (JP | (op_bex & isNotEqual));
	assign pc_sources_2[31: 27] = pc_mux_helper ? 5'b00000 : pc_sources_1[31: 27];
	assign pc_sources_2[26: 0] = pc_mux_helper ? target : pc_sources_1[26: 0];
	assign pc_in = op_jr ? data_readRegB : pc_sources_2;
	
	
	
	// Control Signals
	assign JP = op_j | op_jal;
	assign ALUinB = op_addi | op_sw | op_lw;
	assign ctrl_writeEnable = op_add | op_addi | op_sub | op_and | op_or | op_sll | op_sra | op_lw | op_jal | op_setx;
	assign Rdst = op_sw | op_bne | op_jr | op_blt;


endmodule
