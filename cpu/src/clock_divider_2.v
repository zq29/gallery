module clock_divider_2(clk_in, rst, clk_out);
	output reg clk_out;
	input clk_in;
	input rst;
	
	always@(posedge clk_in or posedge rst) begin
	if(rst) begin
		clk_out <= 1'b0;
	end else
		clk_out <= ~clk_out;	
	end
endmodule