// passed test
module immediate_sx(sx_out, sx_in);
	output[31: 0] sx_out;
	input[16: 0] sx_in;
	
	assign sx_out[31: 17] = sx_in[16] ? 15'b111111111111111 : 15'b000000000000000;
	assign sx_out[16: 0] = sx_in;
	
endmodule
