// doing add / sub(if is_sub_op is true), report overflow
// quick tested with waveform
module adder_32_bit(sum, cout, overflow, a, b, is_sub_op);
	output[31: 0] sum;
	output cout;
	output overflow;
	input[31: 0] a;
	input[31: 0] b;
	input is_sub_op;
	
	// check if do subtraction
	wire[31: 0] not_b;
	wire[31: 0] final_b;
	wire cin;
	not_32_bit _not_32_bit(not_b, b);
	assign final_b = is_sub_op ? not_b : b;
	assign cin = is_sub_op ? 1 : 0;
	
	// do the add / sub
	csa_32_bit _csa_32_bit(sum, cout, overflow, a, final_b, cin);
	
endmodule

// 32 bit csa, no testing
module csa_32_bit(sum, cout, overflow, a, b, cin);
	output[31: 0] sum;
	output cout;
	output overflow;
	input[31: 0] a;
	input[31: 0] b;
	input cin;
	
	// lower 16 bit
	wire lower_carry, foo;
	csa_16_bit lower_add(sum[15: 0], lower_carry, foo, a[15: 0], b[15: 0], cin);
	
	// higher 16 bit
	wire[15: 0] higher_sum_0;
	wire[15: 0] higher_sum_1;
	wire cout_0;
	wire cout_1;
	wire ovf_0;
	wire ovf_1;
	csa_16_bit higher_add_0(higher_sum_0, cout_0, ovf_0, a[31: 16], b[31: 16], 0);
	csa_16_bit higher_add_1(higher_sum_1, cout_1, ovf_1, a[31: 16], b[31: 16], 1);
	
	// select
	assign sum[31: 16] = lower_carry ? higher_sum_1 : higher_sum_0;
	assign cout = lower_carry ? cout_1 : cout_0;
	assign overflow = lower_carry ? ovf_1 : ovf_0;
endmodule

// 16 bit csa, no testing
module csa_16_bit(sum, cout, overflow, a, b, cin);
	output[15: 0] sum;
	output cout;
	output overflow;
	input[15: 0] a;
	input[15: 0] b;
	input cin;
	
	// lower 8 bit
	wire lower_carry, foo;
	csa_8_bit lower_add(sum[7: 0], lower_carry, foo, a[7: 0], b[7: 0], cin);
	
	// higher 8 bit
	wire[7: 0] higher_sum_0;
	wire[7: 0] higher_sum_1;
	wire cout_0;
	wire cout_1;
	wire ovf_0;
	wire ovf_1;
	csa_8_bit higher_add_0(higher_sum_0, cout_0, ovf_0, a[15: 8], b[15: 8], 0);
	csa_8_bit higher_add_1(higher_sum_1, cout_1, ovf_1, a[15: 8], b[15: 8], 1);
	
	// select
	assign sum[15: 8] = lower_carry ? higher_sum_1 : higher_sum_0;
	assign cout = lower_carry ? cout_1 : cout_0;
	assign overflow = lower_carry ? ovf_1 : ovf_0;
endmodule

/*
// to test 8 bit csa
module adder(sum, cout, overflow, a, b);
	output[7: 0] sum;
	output cout;
	output overflow;
	input[7: 0] a;
	input[7: 0] b;
	
	csa_8_bit(sum, cout, a, b, 0);
	//csa_8_bit(sum, cout, a, b, 1);
endmodule
*/

// 8 bit csa, quick tested with waveform
module csa_8_bit(sum, cout, overflow, a, b, cin);
	output[7: 0] sum;
	output cout;
	output overflow;
	input[7: 0] a;
	input[7: 0] b;
	input cin;
	
	// lower 4 bit
	wire lower_carry, foo; // lower bit add won't cause overflow
	rca_4_bit lower_add(sum[3: 0], lower_carry, foo, a[3: 0], b[3: 0], cin);
	
	// higher 4 bit
	wire[3: 0] higher_sum_0;
	wire[3: 0] higher_sum_1;
	wire cout_0;
	wire cout_1;
	wire ovf_0;
	wire ovf_1;
	rca_4_bit higher_add_0(higher_sum_0, cout_0, ovf_0, a[7: 4], b[7: 4], 0);
	rca_4_bit higher_add_1(higher_sum_1, cout_1, ovf_1, a[7: 4], b[7: 4], 1);
	
	// select
	assign sum[7: 4] = lower_carry ? higher_sum_1 : higher_sum_0;
	assign cout = lower_carry ? cout_1 : cout_0;
	assign overflow = lower_carry ? ovf_1 : ovf_0; // if higher bit add ovf, then ovf
endmodule

// 4 bit rca, tested in recitation
module rca_4_bit(sum, cout, overflow/*new added, no testing*/, a, b, cin);
	output[3: 0] sum;
	output cout;
	output overflow;
	input[3: 0] a;
	input[3: 0] b;
	input cin;
	wire _cout[2: 0];
	fa fa_0(sum[0], _cout[0], a[0], b[0], cin);
	fa fa_1(sum[1], _cout[1], a[1], b[1], _cout[0]);
	fa fa_2(sum[2], _cout[2], a[2], b[2], _cout[1]);
	fa fa_3(sum[3], cout, a[3], b[3], _cout[2]);
	// overflow if carry out and carry on to the most significant bit are different
	// so use "xor"
	xor(overflow, cout, _cout[2]);
endmodule

// 1 bit full adder, tested in recitation
module fa(sum, cout, a, b, cin);
	output sum, cout;
	input a, b, cin;
	wire xor_out;
	xor xor_0(xor_out, a, b);
	xor xor_1(sum, xor_out, cin);
	wire and_out_0, and_out_1;
	and and_0(and_out_0, xor_out, cin);
	and and_1(and_out_1, a, b);
	or or_1(cout, and_out_0, and_out_1);
endmodule
