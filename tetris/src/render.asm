# 0: 0
# 1-18: main loop
# 19-26: func reg
# 27,28: a0, a1
# 29,30: v0, v1
# 31: ra

# ---------------------------------------- init -------------------------------------
# Initialize type, x and y
# type = 1

add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0
add $0 $0 $0

addi $1 $0 8
sw $1 $0 1

# x = 6 y = 0
addi $2 $0 6
sw $2 $0 2
sw $0 $0 3

# init score
sw $0 $0 562

# init speed factor 8
addi $1 $0 8
sw $1 $0 563

# ARCHIVED: white background -------------------------------------------------------------------
# addi $1 $0 128
# addi $2 $0 272
# addi $3 $0 1
# @init_white_bg_loop:
#	beq $1 $2 @init_white_bg_loop_end
#	sw $3 $1 0
#	addi $1 $1 1
#	j @init_white_bg_loop
# @init_white_bg_loop_end:


# ---------------------------------------- main loop -------------------------------------
@main:

# ---------------------------------------- score + 1 -------------------------------------
lw $1 $0 562
addi $1 $1 1
sw $1 $0 562

# ---------------------------------------- handle input -------------------------------------
input $1

# up
addi $2 $0 117
beq $1 $2 @rotate_curr_shape

# down
addi $2 $0 114
beq $1 $2 @handle_input_end

# left
addi $2 $0 107
beq $1 $2 @go_left

# right
addi $2 $0 116
beq $1 $2 @go_right

# space
addi $2 $0 41
beq $1 $2 @clear_input

# esc 
#addi $2 118
#beq $1 $2 @

@rotate_curr_shape:

# load type to $3
lw $3 $0 1

# if(type == 1)
addi $1 $0 1
beq $3 $1 @rotate_collision_type1_start
# if(type == 2)
addi $1 $0 2
beq $3 $1 @rotate_collision_type2_start
addi $1 $0 3
beq $3 $1 @rotate_collision_type3_start
addi $1 $0 4
beq $3 $1 @rotate_collision_type4_start
addi $1 $0 5
beq $3 $1 @rotate_collision_type5_start
addi $1 $0 6
beq $3 $1 @rotate_collision_type6_start
addi $1 $0 7
beq $3 $1 @rotate_collision_type7_start
addi $1 $0 8
beq $3 $1 @rotate_collision_type8_start
addi $1 $0 9
beq $3 $1 @rotate_collision_type9_start
addi $1 $0 10
beq $3 $1 @rotate_collision_type10_start
addi $1 $0 11
beq $3 $1 @rotate_collision_type11_start
addi $1 $0 12
beq $3 $1 @rotate_collision_type12_start
addi $1 $0 13
beq $3 $1 @rotate_collision_type13_start
j @handle_input_end

@rotate_collision_type1_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-24
	lw $5 $4 -24
	bgt $5 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type2_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 0 skip
	addi $10 $0 0
	beq $2 $10 @handle_input_end

	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-13
	lw $5 $4 -13
	bgt $5 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end


@rotate_collision_type3_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-24
	lw $5 $4 -24
	bgt $5 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end


@rotate_collision_type4_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 11 skip
	addi $11 $0 11
	beq $2 $11 @handle_input_end
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-1 memloc+1
	lw $5 $4 -1
	lw $6 $4 1
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type -3
	lw $3 $0 1
	addi $3 $3 -3
	sw $3 $0 1
	j @handle_input_end

@rotate_collision_type5_start:
	# Nothing
	j @handle_input_end



@rotate_collision_type6_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-12 memloc-24 memloc-36
	lw $5 $4 -12
	lw $6 $4 -24
	lw $7 $4 -36
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end	
	# else type++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end


@rotate_collision_type7_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 0 or 11 or 10 skip
	addi $10 $0 10
	addi $11 $0 11
	beq $2 $0 @handle_input_end
	beq $2 $10 @handle_input_end
	beq $2 $11 @handle_input_end

	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge  memloc-1 +1 +2 
	lw $5 $4 -1
	lw $6 $4 1
	lw $7 $4 2
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end	
	# else type --
	lw $3 $0 1
	addi $3 $3 -1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type8_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x ==  10 skip
	addi $10 $0 10
	beq $2 $10 @handle_input_end

	# map x y to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge  memloc-11 memloc-10
	lw $5 $4 -11
	lw $6 $4 -10
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type9_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 0 skip
	beq $2 $0 @handle_input_end

	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-24 memloc-25
	lw $5 $4 -24
	lw $6 $4 -25
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type10_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 11 skip
	addi $11 $0 11
	beq $2 $11 @handle_input_end

	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-1 memloc+1 memloc-11
	lw $5 $4 -1
	lw $6 $4 +1
	lw $7 $4 -11
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type11_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-12 memloc-24 
	lw $5 $4 -12
	lw $6 $4 -24
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type = 8
	addi $3 $0 8
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type12_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-11 memloc-23 
	lw $5 $4 -11
	lw $6 $4 -23
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type ++
	lw $3 $0 1
	addi $3 $3 1
	sw $3 $0 1
	j @handle_input_end



@rotate_collision_type13_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# if x == 0 skip
	beq $2 $0 @handle_input_end

	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+1 memloc-13
	lw $5 $4 1
	lw $6 $4 -13
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	# else type --
	lw $3 $0 1
	addi $3 $3 -1
	sw $3 $0 1
	j @handle_input_end


@handle_input_end:



@go_left:
# load x y
lw $2 $0 2
lw $3 $0 3
# map x y  to 128 ~ 271 using function map_xy2block
add $27 $2 $0
add $28 $3 $0
jal @map_xy2block
# load memloc to $4
add $4 $29 $0
lw $1 $0 1
addi $8 $0 1
beq $1 $8 @left_collision_type1
addi $8 $0 2
beq $1 $8 @left_collision_type2
addi $8 $0 3
beq $1 $8 @left_collision_type3
addi $8 $0 4
beq $1 $8 @left_collision_type4
addi $8 $0 5
beq $1 $8 @left_collision_type5
addi $8 $0 6
beq $1 $8 @left_collision_type6
addi $8 $0 7
beq $1 $8 @left_collision_type7
addi $8 $0 8
beq $1 $8 @left_collision_type8
addi $8 $0 9
beq $1 $8 @left_collision_type9
addi $8 $0 10
beq $1 $8 @left_collision_type10
addi $8 $0 11
beq $1 $8 @left_collision_type11
addi $8 $0 12
beq $1 $8 @left_collision_type12
addi $8 $0 13
beq $1 $8 @left_collision_type13

@left_collision_type1:
	lw $5 $4 -2
	lw $6 $4 -13
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_left
@left_collision_type2:
	lw $5 $4 -1
	lw $6 $4 -13
	lw $7 $4 -25
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left
@left_collision_type3:
	lw $5 $4 -1
	lw $6 $4 -14
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_left
@left_collision_type4:
	lw $5 $4 -1
	lw $6 $4 -14
	lw $7 $4 -25
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 1
	beq $2 $8 @handle_input_end
	j @do_go_left
@left_collision_type5:
	lw $5 $4 -1
	lw $6 $4 -13
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left
@left_collision_type6:
	lw $5 $4 -2
	bgt $5 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_left
@left_collision_type7:
	lw $5 $4 -1
	lw $6 $4 -13
	lw $7 $4 -25
	lw $8 $4 -37
	# -37 is left 0
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	bgt $8 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left
@left_collision_type8:
	lw $5 $4 -1
	lw $6 $4 -13
	lw $7 $4 -25
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left
@left_collision_type9:
	lw $5 $4 -1
	lw $6 $4 -13
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left
@left_collision_type10:
	lw $5 $4 -1
	lw $6 $4 -13
	lw $7 $4 -26
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 1
	beq $2 $8 @handle_input_end
	j @do_go_left
@left_collision_type11:
	lw $5 $4 -2
	lw $6 $4 -12
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_left
@left_collision_type12:
	lw $5 $4 -1
	lw $6 $4 -13
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_left
@left_collision_type13:
	lw $5 $4 -1
	lw $6 $4 -13
	lw $7 $4 -24
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	j @do_go_left


@do_go_left:
	lw $3 $0 2
	addi $3 $3 -1
	sw $3 $0 2
	j @handle_input_end

@go_right:
# load x y
lw $2 $0 2
lw $3 $0 3
# map x y  to 128 ~ 271 using function map_xy2block
add $27 $2 $0
add $28 $3 $0
jal @map_xy2block
# load memloc to $4
add $4 $29 $0
lw $1 $0 1
addi $8 $0 1
beq $1 $8 @right_collision_type1
addi $8 $0 2
beq $1 $8 @right_collision_type2
addi $8 $0 3
beq $1 $8 @right_collision_type3
addi $8 $0 4
beq $1 $8 @right_collision_type4
addi $8 $0 5
beq $1 $8 @right_collision_type5
addi $8 $0 6
beq $1 $8 @right_collision_type6
addi $8 $0 7
beq $1 $8 @right_collision_type7
addi $8 $0 8
beq $1 $8 @right_collision_type8
addi $8 $0 9
beq $1 $8 @right_collision_type9
addi $8 $0 10
beq $1 $8 @right_collision_type10
addi $8 $0 11
beq $1 $8 @right_collision_type11
addi $8 $0 12
beq $1 $8 @right_collision_type12
addi $8 $0 13
beq $1 $8 @right_collision_type13

@right_collision_type1:
	lw $5 $4 2
	lw $6 $4 -11
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 10
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type2:
	lw $5 $4 1
	lw $6 $4 -10
	lw $7 $4 -23
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	addi $8 $0 10
	beq $2 $8 @handle_input_end
	j @do_go_right
@right_collision_type3:
	lw $5 $4 1
	lw $6 $4 -10
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 1
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type4:
	lw $5 $4 1
	lw $6 $4 -11
	lw $7 $4 -23
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 11
	beq $2 $8 @handle_input_end
	j @do_go_right
@right_collision_type5:
	lw $5 $4 2
	lw $6 $4 -10
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	beq $2 $0 @handle_input_end
	addi $8 $0 10
	beq $2 $8 @handle_input_end
	j @do_go_right
@right_collision_type6:
	lw $5 $4 3
	bgt $5 $0 @handle_input_end
	addi $7 $0 9
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type7:
	lw $5 $4 1
	lw $6 $4 -11
	lw $7 $4 -23
	lw $8 $4 -35
	# -35 is right 0
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	bgt $8 $0 @handle_input_end
	addi $9 $0 11
	beq $2 $9 @handle_input_end
	j @do_go_right
@right_collision_type8:
	lw $5 $4 2
	lw $6 $4 -11
	lw $7 $4 -23
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 10
	beq $2 $8 @handle_input_end
	j @do_go_right
@right_collision_type9:
	lw $5 $4 1
	lw $6 $4 -9
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 9
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type10:
	lw $5 $4 1
	lw $6 $4 -11
	lw $7 $4 -23
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 11
	beq $2 $8 @handle_input_end
	j @do_go_right
@right_collision_type11:
	lw $5 $4 2
	lw $6 $4 -10
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 10
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type12:
	lw $5 $4 2
	lw $6 $4 -11
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	addi $7 $0 10
	beq $2 $7 @handle_input_end
	j @do_go_right
@right_collision_type13:
	lw $5 $4 1
	lw $6 $4 -10
	lw $7 $4 -22
	bgt $5 $0 @handle_input_end
	bgt $6 $0 @handle_input_end
	bgt $7 $0 @handle_input_end
	addi $8 $0 10
	beq $2 $8 @handle_input_end
	j @do_go_right


@do_go_right:
	lw $3 $0 2
	addi $3 $3 1
	sw $3 $0 2
	j @handle_input_end

@clear_input:
j @handle_input_end


@handle_input_end:








# ---------------------------------------- clear lines -------------------------------------
# $1: curr line index
addi $1 $0 0
addi $2 $0 12
@detect_line_loop:
	beq $1 $2 @detect_line_loop_end

	add $27 $0 $0
	add $28 $1 $0
	jal @map_xy2block
	add $3 $29 $0
	# check line, for addr in $3 to addr + 11
	lw $4 $3 0
	beq $4 $0 @this_line_end
	lw $4 $3 1
	beq $4 $0 @this_line_end
	lw $4 $3 2
	beq $4 $0 @this_line_end
	lw $4 $3 3
	beq $4 $0 @this_line_end
	lw $4 $3 4
	beq $4 $0 @this_line_end
	lw $4 $3 5
	beq $4 $0 @this_line_end
	lw $4 $3 6
	beq $4 $0 @this_line_end
	lw $4 $3 7
	beq $4 $0 @this_line_end
	lw $4 $3 8
	beq $4 $0 @this_line_end
	lw $4 $3 9
	beq $4 $0 @this_line_end
	lw $4 $3 10
	beq $4 $0 @this_line_end
	lw $4 $3 11
	beq $4 $0 @this_line_end

	# exe to here means we need to clear the line

	# score + 50
	lw $10 $0 562
	addi $10 $10 50
	sw $10 $0 562

	# speed up
	lw $10 $0 563
	addi $11 $0 1
	beq $10 $11 @skip_speed_up
	addi $10 $10 -1
	sw $10 $0 563
	@skip_speed_up:

	# clear this line first
	sw $0 $3 0
	sw $0 $3 1
	sw $0 $3 2
	sw $0 $3 3
	sw $0 $3 4
	sw $0 $3 5
	sw $0 $3 6
	sw $0 $3 7
	sw $0 $3 8
	sw $0 $3 9
	sw $0 $3 10
	sw $0 $3 11

	# if this line is the top line
	beq $1 $0 @this_line_end

	# for this line - 1 to the top line (0), drop
	# or equivalently, move prev mem to mem + 12
	# for mem addr $3 - 1 (the last block of the prev line)
	# to mem 128 (see mem documentation)
	addi $5 $3 -1
	addi $6 $0 127
	@drop_line:
		beq $5 $6 @drop_line_end

		lw $7 $5 0
		sw $7 $5 12

		addi $5 $5 -1
		j @drop_line
	@drop_line_end:


	@this_line_end:
	addi $1 $1 1
	j @detect_line_loop
@detect_line_loop_end:








# ---------------------------------------- render -------------------------------------
# STATIC SHAPE -----------------------------------------------------------------------
# $1: y from 0 to 11, $2: x from 0 to 11
addi $1 $0 0
addi $2 $0 0
addi $3 $0 12
@static_render_row_loop:
	beq $1 $3 @static_render_row_loop_end

	add $2 $0 $0
	@static_render_col_loop:
		beq $2 $3 @static_render_col_loop_end

		# get mem addr of a static block
		add $27 $2 $0
		add $28 $1 $0
		jal @map_xy2block
		# get the color of that block
		lw $4 $29 0
		# x at 513, y at 514, color at 515
		sw $2 $0 513
		sw $1 $0 514
		sw $4 $0 515
		jal @render_logic_block

		addi $2 $2 1
		j @static_render_col_loop
	@static_render_col_loop_end:

	addi $1 $1 1
	j @static_render_row_loop
@static_render_row_loop_end:

# DYNAMIC SHAPE -----------------------------------------------------------------------
# read x, y
lw $1 $0 1
lw $2 $0 2
lw $3 $0 3
sw $2 $0 513
sw $3 $0 514


# $2, $3, $4: x, y, color: 513, 514, 515

addi $18 $0 1
beq $1 $18 @dynamic_render_type1
addi $18 $0 2
beq $1 $18 @dynamic_render_type2
addi $18 $0 3
beq $1 $18 @dynamic_render_type3
addi $18 $0 4
beq $1 $18 @dynamic_render_type4
addi $18 $0 5
beq $1 $18 @dynamic_render_type5
addi $18 $0 6
beq $1 $18 @dynamic_render_type6
addi $18 $0 7
beq $1 $18 @dynamic_render_type7
addi $18 $0 8
beq $1 $18 @dynamic_render_type8
addi $18 $0 9
beq $1 $18 @dynamic_render_type9
addi $18 $0 10
beq $1 $18 @dynamic_render_type10
addi $18 $0 11
beq $1 $18 @dynamic_render_type11
addi $18 $0 12
beq $1 $18 @dynamic_render_type12
addi $18 $0 13
beq $1 $18 @dynamic_render_type13

@dynamic_render_type1:
	addi $4 $0 3
	sw $4 $0 515
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	beq $3 $0 @dynamic_render_end
	sw $2 $0 513
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type2:
	addi $4 $0 4
	sw $4 $0 515
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $6 $0 1
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $2 $0 513
	sw $5 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type3:
	addi $4 $0 5
	sw $4 $0 515
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type4:
	addi $4 $0 6
	sw $4 $0 515
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $6 $0 1
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $2 $0 513
	sw $5 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type5:
	addi $4 $0 7
	sw $4 $0 515
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	sw $2 $0 513
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block

	j @dynamic_render_end

@dynamic_render_type6:
	addi $4 $0 8
	sw $4 $0 515
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 2
	sw $5 $0 513
	jal @render_logic_block

	j @dynamic_render_end

@dynamic_render_type7:
	addi $4 $0 9
	sw $4 $0 515
	jal @render_logic_block
	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $6 $0 1
	beq $3 $0 @dynamic_render_end
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $5 $0 514
	jal @render_logic_block
	addi $7 $0 2
	beq $3 $0 @dynamic_render_end
	beq $3 $6 @dynamic_render_end
	beq $3 $7 @dynamic_render_end
	addi $5 $3 -3
	sw $5 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type8:
	addi $4 $0 10
	sw $4 $0 515
	jal @render_logic_block
	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $6 $0 1
	beq $3 $0 @dynamic_render_end
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $5 $0 514
	jal @render_logic_block

	addi $5 $2 1
	sw $5 $0 513
	sw $3 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type9:
	addi $4 $0 11
	sw $4 $0 515
	jal @render_logic_block
	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 2
	sw $5 $0 513
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type10:
	addi $4 $0 12
	sw $4 $0 515
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $6 $0 1
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $2 $0 513
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type11:
	addi $4 $0 13
	sw $4 $0 515
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	beq $3 $0 @dynamic_render_end
	addi $5 $2 1
	sw $5 $0 513
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type12:
	addi $4 $0 14
	sw $4 $0 515
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	sw $2 $0 513

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 -1
	sw $5 $0 513
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_type13:
	addi $4 $0 15
	sw $4 $0 515
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $5 $3 -1
	sw $5 $0 514
	jal @render_logic_block
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block

	beq $3 $0 @dynamic_render_end
	addi $6 $0 1
	beq $3 $6 @dynamic_render_end
	addi $5 $3 -2
	sw $5 $0 514
	addi $5 $2 1
	sw $5 $0 513
	jal @render_logic_block
	j @dynamic_render_end

@dynamic_render_end:

# leaderboard bg ---------------------------------------------------------------
# $1: y from 0 to 11, $2: x from 12 to 15
addi $1 $0 0
addi $2 $0 12
addi $3 $0 12
addi $4 $0 16
# color
addi $5 $0 2
@lb_bg_row_loop:
	beq $1 $2 @lb_bg_row_loop_end

	addi $3 $0 12
	@lb_bg_col_loop:
		beq $3 $4 @lb_bg_col_loop_end

		# x at 513, y at 514, color at 515
		sw $3 $0 513
		sw $1 $0 514
		sw $5 $0 515
		jal @render_logic_block

		addi $3 $3 1
		j @lb_bg_col_loop
	@lb_bg_col_loop_end:

	addi $1 $1 1
	j @lb_bg_row_loop
@lb_bg_row_loop_end:

# show num on leaderboard ---------------------------------------------------------------
lw $1 $0 562
sw $1 $0 561
jal @show_reg_3_digits




# ---------------------------------------- fall -------------------------------------
# collision detect-------------------------------------------------------------------
# load type to $3
lw $3 $0 1

# if(type == 1)
addi $1 $0 1
beq $3 $1 @collision_type1_start
# if(type == 2)
addi $1 $0 2
beq $3 $1 @collision_type2_start
addi $1 $0 3
beq $3 $1 @collision_type3_start
addi $1 $0 4
beq $3 $1 @collision_type4_start
addi $1 $0 5
beq $3 $1 @collision_type5_start
addi $1 $0 6
beq $3 $1 @collision_type6_start
addi $1 $0 7
beq $3 $1 @collision_type7_start
addi $1 $0 8
beq $3 $1 @collision_type8_start
addi $1 $0 9
beq $3 $1 @collision_type9_start
addi $1 $0 10
beq $3 $1 @collision_type10_start
addi $1 $0 11
beq $3 $1 @collision_type11_start
addi $1 $0 12
beq $3 $1 @collision_type12_start
addi $1 $0 13
beq $3 $1 @collision_type13_start
j @collision_end

@collision_type1_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+11 memloc+12 memloc+13
	lw $5 $4 11
	lw $6 $4 12
	lw $7 $4 13
	bgt $5 $0 @archive_shape_type1
	bgt $6 $0 @archive_shape_type1
	bgt $7 $0 @archive_shape_type1
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type1:
		addi $1 $0 3
		sw $1 $4 0
		sw $1 $4 1
		sw $1 $4 -1
		sw $1 $4 -12
		#init next type -> 2 y -> 0
		addi $1 $0 2
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end


@collision_type2_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+1 memloc+12 
	lw $5 $4 1
	lw $6 $4 12
	bgt $5 $0 @archive_shape_type2
	bgt $6 $0 @archive_shape_type2
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type2:
		addi $1 $0 4
		sw $1 $4 0
		sw $1 $4 -11
		sw $1 $4 -12
		sw $1 $4 -24
		#init next type -> 3 y -> 0
		addi $1 $0 3
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type3_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+1 memloc-1 memloc+12 
	lw $5 $4 1
	lw $6 $4 -1
	lw $7 $4 12
	bgt $5 $0 @archive_shape_type3
	bgt $6 $0 @archive_shape_type3
	bgt $7 $0 @archive_shape_type3
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type3:
		addi $1 $0 5
		sw $1 $4 0
		sw $1 $4 -13
		sw $1 $4 -12
		sw $1 $4 -11
		#init next type -> 4 y -> 0
		addi $1 $0 4
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type4_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-1 memloc+12 
	lw $5 $4 -1
	lw $6 $4 12
	bgt $5 $0 @archive_shape_type4
	bgt $6 $0 @archive_shape_type4
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type4:
		addi $1 $0 6
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -13
		sw $1 $4 -24
		#init next type -> 5 y -> 0
		addi $1 $0 5
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type5_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+12 memloc+13
	lw $5 $4 12
	lw $6 $4 13
	bgt $5 $0 @archive_shape_type5
	bgt $6 $0 @archive_shape_type5
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type5:
		addi $1 $0 7
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -11
		sw $1 $4 1
		#init next type -> 6 y -> 0
		addi $1 $0 6
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type6_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+11 memloc+12 memloc+13  memloc+14
	lw $5 $4 11
	lw $6 $4 12
	lw $7 $4 13
	lw $8 $4 14
	bgt $5 $0 @archive_shape_type6
	bgt $6 $0 @archive_shape_type6
	bgt $7 $0 @archive_shape_type6			
	bgt $8 $0 @archive_shape_type6
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type6:
		addi $1 $0 8
		sw $1 $4 0
		sw $1 $4 -1
		sw $1 $4 1
		sw $1 $4 2
		#init next type -> 7 y -> 0
		addi $1 $0 7
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type7_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge  memloc+12 
	lw $6 $4 12
	bgt $6 $0 @archive_shape_type7
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type7:
		addi $1 $0 9
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -24
		sw $1 $4 -36
		#init next type -> 8 y -> 0
		addi $1 $0 8
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type8_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge  memloc+12 memloc+13
	lw $5 $4 12
	lw $6 $4 13
	bgt $5 $0 @archive_shape_type8
	bgt $6 $0 @archive_shape_type8
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type8:
		addi $1 $0 10
		sw $1 $4 0
		sw $1 $4 1
		sw $1 $4 -12
		sw $1 $4 -24
		#init next type -> 9 y -> 0
		addi $1 $0 9
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type9_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+1 memloc+2 memloc+12 
	lw $5 $4 1
	lw $6 $4 2
	lw $7 $4 12
	bgt $5 $0 @archive_shape_type9
	bgt $6 $0 @archive_shape_type9
	bgt $7 $0 @archive_shape_type9
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type9:
		addi $1 $0 11
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -11
		sw $1 $4 -10
		#init next type -> 10 y -> 0
		addi $1 $0 10
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type10_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-1 memloc-13 memloc+12
	lw $6 $4 -13
	lw $7 $4 12
	bgt $6 $0 @archive_shape_type10
	bgt $7 $0 @archive_shape_type10
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type10:
		addi $1 $0 12
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -24
		sw $1 $4 -25
		#init next type -> 11 y -> 0
		addi $1 $0 11
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end


@collision_type11_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+11 memloc+12 memloc+13
	lw $5 $4 11
	lw $6 $4 12
	lw $7 $4 13
	bgt $5 $0 @archive_shape_type11
	bgt $6 $0 @archive_shape_type11
	bgt $7 $0 @archive_shape_type11
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type11:
		addi $1 $0 13
		sw $1 $4 0
		sw $1 $4 -1
		sw $1 $4 1
		sw $1 $4 -11
		#init next type -> 12 y -> 0
		addi $1 $0 12
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type12_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc-1 memloc+12 memloc+13
	lw $5 $4 -1
	lw $6 $4 12
	lw $7 $4 13
	bgt $5 $0 @archive_shape_type12
	bgt $6 $0 @archive_shape_type12
	bgt $7 $0 @archive_shape_type12
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type12:
		addi $1 $0 14
		sw $1 $4 0
		sw $1 $4 1
		sw $1 $4 -12
		sw $1 $4 -13
		#init next type -> 13 y -> 0
		addi $1 $0 13
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_type13_start:
	# load x y
	lw $2 $0 2
	lw $3 $0 3
	# map x y  to 128 ~ 271 using function map_xy2block
	add $27 $2 $0
	add $28 $3 $0
	jal @map_xy2block
	# load memloc to $4
	add $4 $29 $0
	# judge memloc+1 memloc+12 
	lw $5 $4 1
	lw $6 $4 12
	bgt $5 $0 @archive_shape_type13
	bgt $6 $0 @archive_shape_type13
	# else y ++
	lw $3 $0 3
	addi $3 $3 1
	sw $3 $0 3
	j @collision_end

	@archive_shape_type13:
		addi $1 $0 15
		sw $1 $4 0
		sw $1 $4 -12
		sw $1 $4 -11
		sw $1 $4 -23
		#init next type -> 1 y -> 0
		addi $1 $0 1
		sw $1 $0 1
		addi $1 $0 6
		sw $1 $0 2
		sw $0 $0 3
		j @collision_end

@collision_end:



# -------------------------------- detect the end of the game -------------------------
# for loop 104 to 127 , if block != 0 end
addi $1 $0 104
addi $2 $0 128
@detect_end_loop:
	beq $1 $2 @detect_end_loop_end
	# load color to $3
	lw $3 $1 0
	bgt $3 $0 @end_infinitly_loop
	addi $1 $1 1
	j @detect_end_loop
@end_infinitly_loop:
	# deadlock loop
	j @end_infinitly_loop
@detect_end_loop_end:



# ---------------------------------------- sleep -------------------------------------
addi $1 $0 1
addi $2 $0 20
# 2^24 = 16777216 ~= 16M
sll $3 $1 $2
# ESC 118, ENTER 90
addi $5 $0 118
addi $6 $0 90

# speed factor
addi $8 $0 0
lw $9 $0 563

# speed factor
@speed_factor_delay_loop:
	beq $8 $9 @speed_factor_delay_loop_end
	addi $8 $8 1

	addi $1 $0 1
	@sleep_loop:
		beq $1 $3 @sleep_loop_end

		# interupt
		input $4
		beq $4 $5 @stop_game
		j @resume_game

		@stop_game:
			input $7
			beq $7 $6 @resume_game
			j @stop_game

		@resume_game:
		addi $1 $1 1
		j @sleep_loop
	@sleep_loop_end:

j @speed_factor_delay_loop
@speed_factor_delay_loop_end:












j @main










# ---------------------------------------- helper functions -------------------------------------
# positive interger multi, requied: a0 >= a1
@pos_mul:
	sw $19 $0 520
	# i = 0
	add $19 $0 $0
	# res = 0
	add $29 $0 $0
	@pos_mul_loop:
		# if i == b, end loop
		beq $19 $28 @pos_mul_loop_end
		# res += a
		add $29 $29 $27
		# i++
		addi $19 $19 1
		# jump to loop
		j @pos_mul_loop
	@pos_mul_loop_end:
	# return to caller
	lw $19 $0 520
	jr $31

# positive integer div
@pos_div:
	# res = 0
	add $29 $0 $0
	@pos_div_loop:
		# if b > a, break
		bgt $28 $27 @pos_div_loop_end
		# a -= b
		sub $27 $27 $28
		# res++
		addi $29 $29 1
		j @pos_div_loop
	@pos_div_loop_end:
	# mod = a
	add $30 $27 $0
	jr $31 

# Fuction: Map -> Convert (x, y) to a block through x + y * 12 + 128
# Input: a0(27) -> x; a1(28) -> y; Output: v0(29)
@map_xy2block:
	sw $19 $0 284
	sw $20 $0 285
	sw $21 $0 286
	# Firstly store the x y to mem[287] and mem[288]
	sw $27 $0 287
	sw $28 $0 288
	sw $31 $0 289

	# Then using the multi function calculate y * 12
	addi $27 $0 12
	jal @pos_mul
	# $4 = x + $v0 + 128
	lw $19 $0 287
	lw $31 $0 289
	add $21 $19 $29
	addi $21 $21 128
	add $29 $21 $0

	lw $19 $0 284
	lw $20 $0 285
	lw $21 $0 286
	jr $31




# ---------------------------------------- render functions -------------------------------------
@render_logic_block:
	sw $19 $0 521
	sw $20 $0 522
	sw $21 $0 523
	sw $22 $0 524
	sw $23 $0 525
	sw $24 $0 526
	sw $25 $0 527
	sw $26 $0 528
	# mem[513]: x, mem[514]: y, mem[515]: color

	sw $31 $0 516
	# row($19) = y * 40, and ++ each outer loop
	# $20 = (y + 1) * 40
	# $21 = row * 640
	# $22 = 4
	# col($23) = x * 40, and ++ each inner loop
	# $24 = (x + 1) * 40
	# $25: feed to graph mem
	# $26: color

	# 1 -> 40
	addi $27 $0 40
	lw $28 $0 514
	jal @pos_mul
	add $19 $29 $0
	addi $20 $19 40

	# const 4
	addi $22 $0 4

	lw $26 $0 515

	@render_logic_block_row_loop:
		beq $19 $20 @render_logic_block_row_loop_end
		# col = x * 40
		addi $27 $0 40
		lw $28 $0 513
		jal @pos_mul
		add $23 $29 $0
		addi $24 $23 40

		addi $27 $0 640
		add $28 $19 $0
		jal @pos_mul
		add $21 $29 $0

		@render_logic_block_col_loop:
			beq $23 $24 @render_logic_block_col_loop_end
			# addr = row * 640 + col
			add $25 $21 $23
			# addr *= 4
			sll $25 $25 $22
			add $25 $25 $26
			# show it on the screen!
			sw $25 $0 0

			addi $23 $23 1
			j @render_logic_block_col_loop
		@render_logic_block_col_loop_end:
		addi $19 $19 1
		j @render_logic_block_row_loop
	@render_logic_block_row_loop_end:
	lw $31 $0 516
	lw $19 $0 521
	lw $20 $0 522
	lw $21 $0 523
	lw $22 $0 524
	lw $23 $0 525
	lw $24 $0 526
	lw $25 $0 527
	lw $26 $0 528
	jr $31

# a block is 8px * 8px
# so logically, coord goes from (0, 0) to (20, 60)
@fill_board_block:
	sw $19 $0 529
	sw $20 $0 530
	sw $21 $0 531
	sw $22 $0 532
	sw $23 $0 533
	sw $24 $0 534
	sw $25 $0 535
	sw $26 $0 536
	sw $31 $0 537

	# x, y, color
	lw $19 $0 538
	lw $20 $0 539
	lw $21 $0 540

	# physical Y from 8y to 8(y+1) with bias 0
	# physical X from 8x to 8(x+1) with bias 480
	addi $22 $0 3
	sll $20 $20 $22
	addi $22 $20 8
	@fill_board_block_Y_loop:
		beq $20 $22 @fill_board_block_Y_loop_end

		addi $24 $0 3
		sll $23 $19 $24
		addi $23 $23 480
		addi $24 $23 8
		@fill_board_block_X_loop:
			beq $23 $24 @fill_board_block_X_loop_end

			addi $27 $0 640
			add $28 $20 $0
			jal @pos_mul
			add $25 $29 $23
			addi $26 $0 4
			sll $25 $25 $26
			add $25 $25 $21
			sw $25 $0 0

			addi $23 $23 1
			j @fill_board_block_X_loop
		@fill_board_block_X_loop_end:

		addi $20 $20 1
		j @fill_board_block_Y_loop
	@fill_board_block_Y_loop_end:

	lw $19 $0 529
	lw $20 $0 530
	lw $21 $0 531
	lw $22 $0 532
	lw $23 $0 533
	lw $24 $0 534
	lw $25 $0 535
	lw $26 $0 536
	lw $31 $0 537
	jr $31

@show_num:
	sw $19 $0 541
	sw $20 $0 542
	sw $21 $0 543
	sw $22 $0 544
	sw $23 $0 545
	sw $24 $0 546
	sw $25 $0 547
	sw $26 $0 548
	sw $31 $0 549

	# using leaderboard cood system (20 x 60)
	# $19: start x, $20: end x
	# $21: start y, $22: end y
	# $23: mem addr of curr color (3846-4096)
	# $24: color of curr block
	# $25: const temp variable hold start x

	addi $27 $0 5
	lw $28 $0 550
	jal @pos_mul
	add $19 $29 $0

	# x offset 3, y offset 15, width and height = 5
	addi $19 $19 3
	addi $20 $19 5
	addi $21 $0 15
	addi $22 $21 5
	# get mem addr, start from <num> * 25(1 num need 25 blocks) + 3846(offset)
	addi $27 $0 25
	lw $28 $0 551
	jal @pos_mul
	addi $23 $29 3846

	add $25 $19 $0
	@show_num_y_loop:
		beq $21 $22 @show_num_y_loop_end

		add $19 $25 $0
		@show_num_x_loop:
			beq $19 $20 @show_num_x_loop_end

			sw $19 $0 538
			sw $21 $0 539
			lw $24 $23 0
			sw $24 $0 540
			jal @fill_board_block
			addi $23 $23 1

			addi $19 $19 1
			j @show_num_x_loop
		@show_num_x_loop_end:

		addi $21 $21 1
		j @show_num_y_loop
	@show_num_y_loop_end:

	lw $19 $0 541
	lw $20 $0 542
	lw $21 $0 543
	lw $22 $0 544
	lw $23 $0 545
	lw $24 $0 546
	lw $25 $0 547
	lw $26 $0 548
	lw $31 $0 549
	jr $31



@show_reg_3_digits:
	sw $19 $0 552
	sw $20 $0 553
	sw $21 $0 554
	sw $22 $0 555
	sw $23 $0 556
	sw $24 $0 557
	sw $25 $0 558
	sw $26 $0 559
	sw $31 $0 560

	lw $19 $0 561
	add $27 $19 $0
	addi $28 $0 10
	jal @pos_div
	add $19 $29 $0
	addi $20 $0 2
	add $21 $30 $0
	sw $20 $0 550
	sw $21 $0 551
	jal @show_num
	add $27 $19 $0
	addi $28 $0 10
	jal @pos_div
	add $19 $29 $0
	addi $20 $0 1
	add $21 $30 $0
	sw $20 $0 550
	sw $21 $0 551
	jal @show_num
	add $27 $19 $0
	addi $28 $0 10
	jal @pos_div
	add $19 $29 $0
	addi $20 $0 0
	add $21 $30 $0
	sw $20 $0 550
	sw $21 $0 551
	jal @show_num

	lw $19 $0 552
	lw $20 $0 553
	lw $21 $0 554
	lw $22 $0 555
	lw $23 $0 556
	lw $24 $0 557
	lw $25 $0 558
	lw $26 $0 559
	lw $31 $0 560
	jr $31

# ****************************************** ARCHIVED ***************************************
# ---------------------------------------- clear screen -------------------------------------
# i = 0
add $1 $0 $0
# 307200 is b'1001011000000000000, N has only 16 bits, so sll
addi $2 $0 75
addi $3 $0 12
sll $2 $2 $3
# const 4
addi $4 $0 4
@clear_screen_loop:
	# if i == 307,200 break
	beq $1 $2 @clear_screen_loop_end

	# clear
	sll $5 $1 $4
	sw $5 $0 0

	addi $1 $1 1
	j @clear_screen_loop
@clear_screen_loop_end: