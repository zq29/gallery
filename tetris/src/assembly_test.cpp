#include<iostream>
#include<string>
#include<sstream>
#include<vector>
#include<fstream>
#include<iomanip>
#include<map>

#define ASM "render.asm"

using namespace std;

map<string, int> tagMap;


void dec2bin(int num, int bin[], int size){
	for(int i = size; i>=0; i--){
		if(num & (1<<i)) 
			bin[size-1-i]=1;
		else
			bin[size-1-i]=0;
	}
}

int getRegNum(string input){
	input.erase(input.begin());
	int res;
	if(input.compare("rstatus") == 0) res = 30;
	else if(input.compare("ra") == 0) res = 31;
	else res = stoi(input);
	if(res >31 ) cout << "Reg fault!" << endl;
	return res;
}
int compare(string input){
	
	if(input.compare("add") == 0) return 0;
	else if(input.compare("sub") == 0) return 1;
	else if(input.compare("and") == 0) return 2;
	else if(input.compare("or") == 0) return 3;
	else if(input.compare("sll") == 0) return 4;
	else if(input.compare("srl") == 0) return 5;
	else if(input.compare("addi") == 0) return 6;
	else if(input.compare("lw") == 0) return 7;
	else if(input.compare("sw") == 0) return 8;
	else if(input.compare("beq") == 0) return 9;
	else if(input.compare("bgt") == 0) return 10;
	else if(input.compare("jr") == 0) return 11;
	else if(input.compare("j") == 0) return 12;
	else if(input.compare("jal") == 0) return 13;
	else if(input.compare("input") == 0) return 14;
	else if(input.compare("output") == 0) return 15;
	else{
		cerr << "In ins " << input << "opcode fault!" << endl;
		throw exception();
		return 31;
	}
}

int main ()
{
	ifstream fin(ASM, ifstream::in);  
	ofstream fout("imem.mif");
	fout << "DEPTH = 4096;" << endl <<
	"WIDTH = 32;" << endl <<
	"ADDRESS_RADIX = DEC;" << endl <<
	"DATA_RADIX = BIN;" << endl << 
	"CONTENT" << endl <<
	"BEGIN" << endl;
	string insn;
	int lineNum = 0;
	int insnCount = 0;
	
	
	
	while (getline (fin,insn)) {
		if(insn == "") continue;
		while(insn[0] == '\t') {
			insn = insn.substr(1, insn.length());
		}
		if(insn[0] == '#'){}
		else if(insn[0] == '@') {
			tagMap[insn.substr(0, insn.length() - 1)] = insnCount;
			cout << "FOUND TAG " << insn.substr(0, insn.length() - 1) << " " << insnCount << endl;
		} 
		else {
			insnCount++;
		}//end else

	}
	fin.close();
	fin.open(ASM);
	insnCount = 0;
	
	
	
	
	while (getline (fin,insn)) {
		if(insn == "") continue;
		while(insn[0] == '\t') {
			insn = insn.substr(1, insn.length());
		}
		if(insn[0] == '#'){}
		else if(insn[0] == '@') {} 
		else {
			cout << insn << endl;
			
					fout<<setw(4)<<setfill('0')<<lineNum++<<" : ";
					vector<string> res;
					string result;
					stringstream input(insn);
					while(input>>result){
						res.push_back(result);
					}
					 
					
					int type = compare(res[0]);
					
					//if R
					if (type >=0 && type <=5) {
						int opcode[5]={0};
						int Rd[5]={0};
						int Rs[5]={0};
						int Rt[5]={0};
						dec2bin(type, opcode, 5);
						dec2bin(getRegNum(res[1]), Rd, 5);
						dec2bin(getRegNum(res[2]), Rs, 5);
						dec2bin(getRegNum(res[3]), Rt, 5);
						for(int i = 0; i < 5; i++) fout << opcode[i];
						for(int i = 0; i < 5; i++) fout << Rd[i];
						for(int i = 0; i < 5; i++) fout << Rs[i];
						for(int i = 0; i < 5; i++) fout << Rt[i];
						fout << "000000000000";
						
					}//if R
					
					//if I
					if (type >=6 && type <=10) {
						int opcode[5]={0};
						int Rd[5]={0};
						int Rs[5]={0};
						int Immed[17]={0};
						dec2bin(type, opcode, 5);
						dec2bin(getRegNum(res[1]), Rd, 5);
						dec2bin(getRegNum(res[2]), Rs, 5);
						for(int i = 0; i < 5; i++) fout << opcode[i];
						for(int i = 0; i < 5; i++) fout << Rd[i];
						for(int i = 0; i < 5; i++) fout << Rs[i];
						
						if(type == 9 || type == 10) {
							if(res[3][0] != '@') {
								cerr << "wrong ins " << insn << endl;
								throw invalid_argument("wrong ins ");
							}
							if(tagMap.find(res[3]) == tagMap.end()) {
								cerr << insn << endl;
								throw invalid_argument("invalid insn!");
							}
							dec2bin(tagMap[res[3]] - insnCount - 1, Immed, 17);
	
						} else {
							dec2bin(stoi(res[3]), Immed, 17);
						}
						for(int i = 0; i < 17; i++) fout << Immed[i];
						
						
					}//if I
					
					//if jr input output
					if (type == 11 || type == 14 || type == 15) {
						int opcode[5]={0};
						int Rd[5]={0};
						int Rs[5]={0};
						string Immed="00000000000000000";
						dec2bin(type, opcode, 5);
						dec2bin(getRegNum(res[1]), Rd, 5);
						for(int i = 0; i < 5; i++) fout << opcode[i];
						for(int i = 0; i < 5; i++) fout << Rd[i];
						for(int i = 0; i < 5; i++) fout << Rs[i];
						fout << Immed;
						//for(int i = 0; i < 17; i++) fout << Immed[i];
					}//if jr
					
					//if J
					if (type ==12 || type ==13) {
						int opcode[5]={0};
						int Target[27]={0};
						dec2bin(type, opcode, 5);
						for(int i = 0; i < 5; i++) fout << opcode[i];
						try {
							dec2bin(stoi(res[1]), Target, 27);
						} catch(...) {
							if(tagMap.find(res[1]) == tagMap.end()) {
								cerr << insn << endl;
								throw invalid_argument("invalid insn!");
							}
							dec2bin(tagMap[res[1]], Target, 27);
						}
						for(int i = 0; i < 27; i++) fout << Target[i];
					}//if J
										
			//		fout << "type: " << type << endl;
					fout << "; -- " << insn << endl << endl;
					insnCount++;
		}//end else
		
	}
	
	fout << "END;" << endl;
	
	return 0;
}
