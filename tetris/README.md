# Tetris in Verilog & ASM

Dec 10 2019, an Duke ECE 550 project

<img src="demo.png" width="400px">

Check the demo video [demo.mp4](./demo.mp4)

We wrote this Tetris with Verilog and ASM tailored for the special CPU (I also have my own implemented CPU, [please check it here](../cpu/)) in the FPGA. In this project, we implemented:

* Basic Tertis 
  * 5 different falling shapes
  * Clear line after one is filled with blocks
  * Block rotation
* VGA screen display
  * Defined our own graphic memory
  * Show score board on the screen

* PS2 keyboard I/O - use keyboard to control blocks
* Interrupt - press space to pause the game
* Speed Power-up - falling faster after achieving high scores

### Detail about this project

* You can find everything needed to compile the game in dir `Tetris_P_restored`

* Instructions to play this game:
  * IMPORTANT: you must press "space" to make it a one-time input, or it will repetitive use this input
    	e.g. after press "→", the block keeps moving to the right until you press "space"
  * Left & right: use ← & →
  * Rotate: ↑
  * Pause & resume game: "Esc" & "Enter"
  * Scores on the leaderboard plus 1 every frame, and plus 50 after a line is cleared

* If you're interested in how we implemented the game, please read further:
  Since the whole game is written in assembly, you may want to check the implementation in
  * ./src/render.asm 				// all code for the game
  * ./Tetris_P_restored/imem.mif 	// you can check machine code & assembly here
* Some interesting utils can be found in dir "src":
  * "assembly_test.cpp": we use this to compile assembly to machine code (to imem.mif)
  * "pixelNum.psd": here is a Photoshop file showing how we display numbers in piexl
  * "num2pix.cpp": we use this to transfer piexel info to data in the dmem.mif
  * "550 Projc Asm Rule.pdf": here it show how we use D-Mem in the assembly