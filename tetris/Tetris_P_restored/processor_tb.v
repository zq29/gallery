`timescale 1 ns / 100 ps

module processor_tb();
	reg clock, reset;
	
	skeleton my_skeleton(.resetn(~reset), .CLOCK_50(clock));

	initial begin
		clock = 0;
		reset = 1;
		@(negedge clock)
		reset = 0;
		# 3000
		$stop;
	end

	// Clock generator
	always
		#10	clock = ~clock;

endmodule
