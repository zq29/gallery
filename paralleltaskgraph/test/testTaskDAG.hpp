#ifndef ZQ_TESTTASKDAG
#define ZQ_TESTTASKDAG

#include "../taskdag.hpp"
#include <assert.h>

namespace zqTestTaskDAG {
namespace zqTestTaskDAGInner {
	using namespace std;
	using namespace zq;
	
	void _testCircle() {
		TaskDAG<int> g;
		int p0 = g.addPlaceholder(0);
		try {
			int op1 = g.addOperation([](vector<int> args){return args[0];}, {1/*op2*/});
			assert(false);
		} catch(...) {}
	}
	
	void _runSimpleExam() {
		TaskDAG<int> g;
		int p0 = g.addPlaceholder(0);
		g.run(p0);
		int p1 = g.addPlaceholder(1);
		int p2 = g.addPlaceholder(2);
		int op3 = g.addOperation([](vector<int> args){return args[0];}, {p0});
		int op4 = g.addOperation([](vector<int> args){return args[0] + args[1] + args[2];}, {p0, p1, p2});
		int op5 = g.addOperation([](vector<int> args){return args[0] + args[1];}, {p2, op3});
		int op6 = g.addOperation([](vector<int> args) {
			for(int e : args) {cout << e << " ";}
			cout << endl;
			return 0;
		}, {p0, p1, p2, op3, op4, op5});
		g.run(op6);
		cout << endl << endl;
		
		g.run(op6);
		cout << endl << endl;
		
		g.setPlaceholder(p0, 100);
		g.run(op6);
		cout << endl << endl;
	}
	
	void _runSimpleExamWithStrategy() {
		TaskDAG<int> g;
		int p0 = g.addPlaceholder(0);
		g.run(p0);
		int p1 = g.addPlaceholder(1);
		int p2 = g.addPlaceholder(2);
		int op3 = g.addOperation([](vector<int> args){return args[0];}, {p0});
		int op4 = g.addOperation([](vector<int> args){return args[0] + args[1] + args[2];}, {p0, p1, p2});
		int op5 = g.addOperation([](vector<int> args){return args[0] + args[1];}, {p2, op3});
		int op6 = g.addOperation([](vector<int> args) {
			for(int e : args) {cout << e << " ";}
			cout << endl;
			return 0;
		}, {p0, p1, p2, op3, op4, op5});
		g.run(op6);
		cout << endl << endl;
		
		g.run(op6);
		cout << endl << endl;
		
		g.setPlaceholder(p0, 100);
		g.run(op6);
		cout << endl << endl;
	}
	
	void _testStrayegy() {
		TaskDAG<int> g;
		int p0 = g.addPlaceholder(0);
		int p1 = g.addPlaceholder(1);
		int p2 = g.addPlaceholder(2);
		int op3 = g.addOperation([](vector<int> args){return args[0];}, {p0});
		int op4 = g.addOperation([](vector<int> args){return args[0] + args[1] + args[2];}, {p0, p1, p2});
		int op5 = g.addOperation([](vector<int> args){return args[0] + args[1];}, {p2, op3});
		int op6 = g.addOperation([](vector<int> args) {
			for(int e : args) {cout << e << " ";}
			cout << endl;
			return 0;
		}, {p0, p1, p2, op3, op4, op5});
		
		g.setStrategy(TaskDAG<int>::Strategy::s1);
		g.run(op6);
		
		// invalid all
		g.setPlaceholder(p0, 0);
		g.setPlaceholder(p1, 1);
		g.setPlaceholder(p2, 2);
		g.setStrategy(TaskDAG<int>::Strategy::s2);
		g.run(op6);
		
		// invalid all
		g.setPlaceholder(p0, 0);
		g.setPlaceholder(p1, 1);
		g.setPlaceholder(p2, 2);
		g.setStrategy(TaskDAG<int>::Strategy::s3);
		g.run(op6);
	}

	void testTaskDAG() {
		_testCircle();
		_runSimpleExam();
		_runSimpleExamWithStrategy();
		_testStrayegy();
	}
}
	using zqTestTaskDAGInner::testTaskDAG;
}

#endif
