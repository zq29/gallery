#ifndef ZQ_TESTTHREADPOOL
#define ZQ_TESTTHREADPOOL

#include <assert.h>
#include <ctime>
#include <cstdlib>
#include <chrono>

#include "../task.hpp"
#include "../threadpool.hpp"

namespace zqTestThreadPool {
namespace zqTestThreadPoolInner {
	using namespace std;
	using namespace zq;
	
	void _testThreadPool() {
		ThreadPool tp(8);
		srand(time(nullptr));
		auto randomSleep = [&]() {
			srand(time(nullptr));
			unsigned ms = rand() % 300;
			this_thread::sleep_for(chrono::milliseconds(ms));
		};
		unsigned nTasks = 16 + rand() % 48;
		for(unsigned i = 0; i < nTasks; i++) {
			tp.feed(randomSleep);
			if(i == 15) {
				tp.waitAll();
			}
		}
		tp.finish();
	}
	
	void _testTPWithTaskDAG() {
		TaskDAG<int> g;
		int p0 = g.addPlaceholder(100);
		int p1 = g.addPlaceholder(200);
		int p2 = g.addPlaceholder(300);
		int p3 = g.addPlaceholder(300);
		int op4 = g.addOperation([](vector<int> a) {
			Log::verbose(Log::msg(a[0], a[1]));
			return a[0] + a[1];
		}, {p0, p1});
		int op5 = g.addOperation([](vector<int> a) {
			Log::verbose(Log::msg(a[0], a[1]));
			return a[0] + a[1];
		}, {p2, p3});
		/*
		int op6 = g.addOperation([](vector<int> a) {
			Log::verbose(Log::msg(a[0], a[1]));
			return a[0] + a[1];
		}, {p2, p0});
		*/
		int op7 = g.addOperation([](vector<int> a) {
			Log::verbose(Log::msg(a[0], a[1]));
			return a[0] + a[1];
		}, {op4, op5});
		g.setStrategy(TaskDAG<int>::Strategy::s2);
		g.run(op7);
	}
	
	void testThreadPool() {
		_testThreadPool();
		_testTPWithTaskDAG();
	}
}
	using zqTestThreadPoolInner::testThreadPool;
}
#endif
