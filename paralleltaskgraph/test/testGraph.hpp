#ifndef ZQ_TESTGRAPH
#define ZQ_TESTGRAPH

#include "../graph.hpp"
#include <assert.h>

namespace zqTestGraph {
namespace zqTestGraphInner {
	using namespace std;
	using namespace zq;
	
	class _tNode1 {
	public:
		int x;
	};
	
	void _testType() {
		DirectedGraph<_tNode1> g;
		_tNode1 n;
		g.addNode(move(n));
	}
	
	void _testAdd() {
		DirectedGraph<int> g;
		int id1 = 3, id2 = 4;
		
		try {
			g.addEdge(1, 2);
			assert(false);
		} catch(...) {}
		
		id1 = g.addNode(392048);
		try {
			g.addEdge(id1, 2);
			assert(false);
		} catch(...) {}
		try {
			g.addEdge(2, id1);
			assert(false);
		} catch(...) {}
		
		id2 = g.addNode(2);
		try {
			g.addEdge(id1, id2);
		} catch(...) {
			assert(false);
		}
		
		Log::info(g.toStr());
	}
	
	void _testGetter() {
		DirectedGraph<int> g;
		
		try {
			g.getNodeRef(0);
			assert(false);
		} catch(...) {}
		
		try{
			g.getAdjs(239897);
			assert(false);
		} catch(...) {}
		int id1 = g.addNode(1);
		
		for(int e : g.getAdjs(id1)) {
			cout << e << " ";
		}
		cout << endl;
		
		int id2 = g.addNode(2);
		g.addEdge(id1, id2);
		int count = 0;
		for(auto e : g.getAdjs(id1)) {
			count++;
		}
		assert(count == 1);
		
		assert(g.getInnodes(id1).size() == 0);
		assert(g.getInnodes(id2).size() == 1);
		for(auto e : g.getInnodes(id2)) {
			cout << e << endl;
		}
	}
	
	void _testDAG() {
		DAG<int> dag;
		assert(!dag.hasCircle());
		
		int id1 = dag.addNode(100);
		int id2 = dag.addNode(200);
		dag.addEdge(id1, id2);
		assert(!dag.hasCircle());
		cout << Log::seq2str(dag.topologicalSort()) << endl;
		
		auto dag2(dag);
		try {
			dag2.addEdge(id2, id1);
			assert(false);	
		} catch(...) {}
		
		int id3 = dag.addNode(300);
		int id4 = dag.addNode(400);
		int id5 = dag.addNode(500);
		dag.addEdge(id3, id2);
		dag.addEdge(id4, id2);
		dag.addEdge(id3, id4);
		dag.addEdge(id5, id4);
		assert(!dag.hasCircle());
		cout << Log::seq2str(dag.topologicalSort()) << endl;
		
		try {
			dag.addEdge(id2, id5);
			assert(false);
		} catch(...) {}
	}
	
	void testGraph() {
		_testType();
		_testAdd();
		_testGetter();
		_testDAG();
	}
}
	using zqTestGraphInner::testGraph;
}

#endif
