#ifndef ZQ_TESTANY
#define ZQ_TESTANY

#include "../any.hpp"
#include "../taskdag.hpp"
#include <assert.h>
#include <memory>

namespace zqTestAny {
namespace zqTestAnyInner {
	using namespace std;
	using namespace zq;
	
	void testAny() {
		TaskDAG<Any> g;
		int p0 = g.addPlaceholder(Any(2));
		int p1 = g.addPlaceholder(Any(5.5));
		int op1 = g.addOperation([](const vector<Any>& args) {
			int a = args[0].get<int>();
			double b = args[1].get<double>();
			return Any((int)(a * b));
		}, {p0, p1});
		
		g.run(op1);
		cout << g.getResult(op1).get<int>() << endl;
	}
}
	using zqTestAnyInner::testAny;
}

#endif
