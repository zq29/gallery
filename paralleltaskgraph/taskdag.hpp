#ifndef ZQ_TASKDAG
#define ZQ_TASKDAG

#include <memory>
#include <unordered_map>
#include <tuple>
#include <set>
#include <type_traits>
#include <thread>

#include "task.hpp"
#include "graph.hpp"
#include "strategy.hpp"

namespace zq {
namespace zqInner {
	using namespace std;
	
	/*
	* assumption about <T>:
	* 	1. operator= is defined
	* 	2. R(const R& rhs) is defined
	*	3. must not be <void>, which is required by <Task>, otherwise causes compile error
	*/
	template<typename T>
	class TaskDAG : public DAG<shared_ptr<Task<T>>>, public virtual Loggable {
	private:
		typedef DAG<shared_ptr<Task<T>>> Parent; // name is not clear and too long
		unique_ptr<TaskDAGStrategy<T>> strategy;
		/*
		* set the whole dependant path invalid, DFS
		*/
		void invalidPath(const int startId, set<int>& vis) {
			Log::verbose(*this, Log::msg("set node ", startId, " invalid"));
			Parent::getNodeCopy(startId)->setValid(false);
			vis.insert(startId);
			for(const int next : Parent::getAdjs(startId)) {
				if(vis.find(next) == vis.end()) {
					invalidPath(next, vis);
				}
			}
		}
		
		shared_ptr<const T> getResPtr(int id) {
			return Parent::getNodeCopy(id)->getResPtr();
		}
	public:
		enum Strategy {
			singleThread, s1,
			threadPool4Topology, s2,
			threadPool8Topology, s3
		};
		/*
		* hide <Task> constructor details with a better name
		*/
		int addPlaceholder(const T& r = T()) {
			return Parent::addNode(make_shared<Task<T>>(r));
		}
		/*
		* offer a better function name
		*/
		void setPlaceholder(int id, const T& r) {
			auto task = Parent::getNodeCopy(id); // a pointer, so a copy would be fine
			if(!task->isPlaceholder()) {
				const string msg = Log::msg(
					"in function <setPlaceholder>, node with id ",
					id, " is not a placeholder!"
				);
				Log::error(msg);
				throw invalid_argument(msg);
			}
			task->setValue(r);
			// set the whole dependant path invalid
			set<int> vis;
			invalidPath(id, vis);
			task->setValid(true);
		}
		
		void setStrategy(Strategy s) {
			switch(s) {
				case singleThread: case s1:
					strategy = unique_ptr<TaskDAGStrategy<T>>(
						new SingleThreadStrat<T>);
					break;
				case threadPool4Topology: case s2:
					strategy = unique_ptr<TaskDAGStrategy<T>>(
						new TopologyThreadPoolStrat<T, 4>);
					break;
				case threadPool8Topology: case s3:
					strategy = unique_ptr<TaskDAGStrategy<T>>(
						new TopologyThreadPoolStrat<T, 8>);
					break;
				default: break;
			}
		}
		
		/*
		* run single operation, no dependency check, no recursion
		* used to improve performance, e.g. run parallelly 
		* if you're not sure if dependencies are ready (valid)
		* then use <runOperation> instead
		*/
		const T& runSingleOperation(int id) {
			auto pTask = Parent::getNodeCopy(id);
			// get args
			vector<T> args;
			for(int e : Parent::getInnodes(id)) {
				if(isResValid(e)) {
					args.push_back(getResult(e));
				} else {
					const string msg = Log::msg(
						"in function <runSingleOperation>, ",
						"while running node ", id,
						" result from node ", e,	
						" is not ready!"
					);
					Log::error(*this, msg);
					throw logic_error(msg);
				}
			}
			pTask->run(args);
			return getResult(id);
		}
		
		/*
		* if dependencies are not ready (valid), recursively run them
		*/
		const T& runOperation(int id) {
			auto pTask = Parent::getNodeCopy(id);
			// get dependencies
			vector<T> args;
			for(int e : Parent::getInnodes(id)) {
				if(isResValid(e)) {
					args.push_back(getResult(e));
				} else {
					args.push_back(runOperation(e));
				}
			}
			pTask->run(args);
			return getResult(id);
		}
		
		/*
		* <f>'s argument <vector<T>> will be constructed at graph's runtime
		* from <depIds> nodes' results
		*/
		int addOperation(const function<T(vector<T>)>& f, const vector<int>& depIds) {
			const int opId = Parent::addNode(make_shared<Task<T>>(f));
			// add dependencies
			for(int e : depIds) {
				Parent::addEdge(e, opId);
			}
			return opId;
		}
		
		/*
		* run with strategy
		*/
		const T& run(int id) {
			if(strategy) {
				return strategy->run(this, id);
			}
			return runOperation(id);
		}
		
		/*
		* A node's value is valid or not is determined by its children. 
		* If no modification was made since last time running this 
		* operation node, then it keeps valid. Any change of value to 
		* its child nodes will make this node invalid
		*/
		bool isResValid(int id) {
			return Parent::getNodeCopy(id)->isResValid();
		}
		
		const T& getResult(int id) {
			return *getResPtr(id);
		}
		
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "TaskDAG";
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};

}
	using zqInner::TaskDAG;
}
	
#endif
