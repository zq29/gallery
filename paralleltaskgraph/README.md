# Parallel Task Graph

Dec 05 2019, Duke ECE 751 final project

## What is Parallel Task Graph?

> A **Task Graph**, in this case, is defined as a graph whose node is a task (function), which takes all its children’s output as its input (arguments of the function).

 Consider the following code:

```c++
int main() {
    int a = 1, b = 2;
    int x = a + a, y = a * b;
    cout << a << b << x << y << endl;
}
```

A possible corresponding task graph is:

```mermaid
graph TD

_1(1)
_2(2)
a(a)
b(b)
x2("+ (x)")
y("* (y)")
print(print)

_1-->a
_2-->b

a-->x2
a-->x2
a-->y
b-->y


a-->print
b-->print
y-->print

x2-->print
```

We can see the potential parallelism in it: we can do operation `+` and operation `*` and the same time since they are independent tasks. 

It is obviously a Directed Acyclic Graph (DAG). In fact, we are only talking about DAG in this project.

> In this context, **Parallel Task Graph** is defined as a Task Graph that is directed and acyclic, and has potential parallelism in it.

## Why do we need a Parallel Task Graph?

#### Run Faster!

We could use multi-thread to run those independent tasks, which makes things faster.

#### Skip Unnecessary Tasks and get... Faster!

When changing input `b` to `200`, you'll find that operation `+` is not actually affected, so we skip this task while re-running this task graph. 

#### Parallel Computing Framework for General Problems

You don't need to write task-specific parallel code, just show us the dependency, we will do synchronization like `waitAll` for you.

## How to use it?

#### Hello-world Example

```c++
TaskDAG<int> g;

int placeholderId0 = g.addPlaceholder(1);
int placeholderId1 = g.addPlaceholder(2);

int opAddId = g.addOperation([](const vector<int>& args) {
	return args[0] + args[1];	
}, {placeholderId0, placeholderId1});

g.runOperation(opAddId);

cout << g.getResult(opAddId) << endl;
```

The code above creates a task graph like this:

```mermaid
graph TD
p1("placeholderId0<br>(default=1)")
p2("placeholderId1<br>(default=2)")
op("opAddId<br>(operation ADD)")
p1-->op
p2-->op
```



Let's go through this line by line.

```c++
TaskDAG<int> g;
```

Create a Parallel Task Graph with type `int`, which means all nodes take `int`s as input and output `int`.

```c++
int placeholderId0 = g.addPlaceholder(1);
int placeholderId1 = g.addPlaceholder(2);
```

Add placeholders to the graph, a placeholder is a node with no actual task but to hold a value.

```c++
int opAddId = g.addOperation([](const vector<int>& args) {
	return args[0] + args[1];	
}, {placeholderId0, placeholderId1});
```

Add an operation (ADD) to the graph with a lambda as its first argument. The lambda takes `vector<int>` as its argument, which is constructed from the second argument, **that is, `args[0]` is the value of node `placeholderId0` and `args[1]` is the value of `placeholderId1`.**

The second argument indicates which nodes it takes input from.

```c++
g.runOperation(opAddId);
```

No actual task was done before this line, we were just building the graph. And with `runOperation` call, data starts to flow in the graph and we are doing ADD at this moment.

```c++
cout << g.getResult(opAddId) << endl;
```

We get the result of this task and print it.

#### Interfaces

* Create object

  ```c++
  template<typename T>
  class TaskDAG;
  ```

  No fancy constructors.

  * `T` is the type that will be used for all placeholders and args and return value of operations. What if I want to use mixed types for an operation, say, take a `string` and an `int` as arguments? See below for `Any`.

* Add element to graph

  There are 2 kinds of nodes in the graph: `placeholder` and `operation`.

  ```c++
  int addPlaceholder(const T& r = T());
  ```

  >A placeholder is a node with no actual task but to hold a value.

  * `r`: default value of the placeholder.
  * return: id of this node.

  ```c++
  int addOperation(const function<T(vector<T>)>& f, const vector<int>& depIds);
  ```

  > Operation has inputs, which must from placeholders or other operations.

  * `f`: a function that takes `vector<T>` as argument, which will be constructed and passed automatically to the function when `runOperation` or similar functions are called, and return `T`, which will be cached and can be fetched by calling `getResult`
  * `depIds`: ids of nodes which offers input to this operation, see examples above. 
  * return: id of this operation.

  Recall what we are only talking about **DAG**s, so what will happen if you add a node and some edges and it makes the graph contains a circle? Well, an exception will be thrown and program aborts. So **add node to a graph may throw unhandled exceptions!** Design your graph carefully.

* Get & Set the value of a node

  ```c++
  void setPlaceholder(int id, const T& r);
  ```

  * Set the value of a placeholder with id `id`

  ```c++
  const T& getResult(int id);
  ```

  * Return the value of a node.

  ```c++
  bool isResValid(int id);
  ```

  * A node's value is valid or not is determined by its children. If no modification was made since last time running this operation node, then it keeps valid. Any change of value to its child nodes will make this node invalid.

* Strategy

  There are currently 3 strategies to run this graph, and they are:
  
  * `TaskDAG<T>::Strategy::singleThread`: run this graph using recursion in a single thread
  * `TaskDAG<T>::Strategy::threadPool4Topology`: after topological sort, run graph with 4 threads
  * `TaskDAG<T>::Strategy::threadPool8Topology`: after topological sort, run graph with 8 threads
  
  Detailed description can be found below in `Parallelization`.
  
  You can set strategy using:
  
  ```c++
  void setStrategy(Strategy s);
  ```
  
* Run an operation

  3 methods are here for you to evaluate a node, they are:

  ```c++
  const T& runSingleOperation(int id);
  ```

  This method runs a single operation, no dependency check, no recursion, it's that simple.

  * used to improve performance, e.g. run parallelly 
  * if you're not sure if dependencies are ready (valid) then use `runOperation` instead

  ```c++
  const T& runOperation(int id);
  ```

  If dependencies are not ready (valid), recursively run them. This guarantees that you will get a valid result. However you don't want this if you want to run with fancy strategies, instead, use:

  ```c++
  const T& run(int id);
  ```

  This also guarantees you get a valid result.

  If you don't know which to choose, use `run`. And... nothing will happen if you run a placeholder.

##Any

It would be boring if your graph could handle only a specific type, like `int`, then you could do limited kinds of tasks and may need to write another level of indirection yourself. Well, I've added that for you!

The idea comes from Boost.Any (<https://www.boost.org/doc/libs/1_61_0/doc/html/any.html>)

Class `Any` stores can any type you want. An example of using `Any` and `TaskDAG`:

```c++
TaskDAG<Any> g;
int p0 = g.addPlaceholder(Any(string("a string")));
int p1 = g.addPlaceholder(Any(2));
int op0 = g.addOperation([](const vector<Any>& args) {
	string s = args[0].get<string>();
	int a = args[1].get<int>();
	return Any(1.0 * s.length() / a);
}, {p0, p1});
g.run(op0);
cout << g.getResult(op0).get<double>() << endl;
```

#### Common Interface

```c++
template<typename T>
Any(T&& d = T());
```

Templated constructor.

```c++
template<typename T>
const T& get() const;
```

Templated getter.

Use `shared_ptr<void>` inside to hide type, and it is your responsibility to keep the type consistent. 

#### About Types

Thanks to `Any`, we could create the graph dynamically at the run time! If using templates instead of `Any`, things would become a lot more complex.

##Parallelization

 #### Single Thread

Using depth first search (DFS) to run the dataflow of a graph.

#### ThreadPool with Topological Sort

Topological sort uses indegree to sort a graph:

```c++
...
while(...) {
    ...
    find all nodes with indegree 0;
    put them in result seq;
    remove them and update indegrees;
 	...
}
...
```

Note that for each iteration, nodes with indegree 0 (together, we call them a `batch`) are independent to each other and thus can be done parallelly. Batches run sequentially and parallelization is achieved within a batch.  So it would be like this:

```c++
for each batch in the result of the modified topological sort:
	for each task in the batch:
		enqueue task to thread pool;
	end
	// or whatever parallel running methods here
	wait for all tasks in this batch to finish;
end
```

#### ThreadPool

Similar to assignment 068, but simplified and improved.

## TF-IDF: an example of TaskGraph application

A brief introduction of TF-IDF can be found here (http://www.tfidf.com/)

If we want to measure the similarity of documents, say `text1.txt`, `text2.txt`, `text3.txt`, we could use `TF-IDF` vector to represent a document and use cosine similarity to measure them. Before that, we need to vectorize the text file with a dictionary. So the TaskGraph may look like this:

```mermaid
graph TD
l01((dict.txt))
l02((text1.txt))
l03((text2.txt))
l04((text3.txt))

l11(dictFileName)
l12(filename1)
l13(filename2)
l14(filename3)

l01-->l11
l02-->l12
l03-->l13
l04-->l14

l21(readDict)

l31(vectorizeFile1)
l32(vectorizeFile2)
l33(vectorizeFile3)

l11-->l21
l12-->l31
l13-->l32
l14-->l33
l21-->l31
l21-->l32
l21-->l33

l41(getIDF)
l42(getTF1)
l43(getTF2)
l44(getTF3)

l31-->l41
l32-->l41
l33-->l41
l31-->l42
l32-->l43
l33-->l44

l51(getTF-IDF1)
l52(getTF-IDF2)
l53(getTF-IDF3)
l42-->l51
l43-->l52
l44-->l53
l41-->l51
l41-->l52
l41-->l53

l61(getSimilarity1)
l62(getSimilarity2)
l51-->l61
l52-->l61
l52-->l62
l53-->l62
```

(Well, there are something wrong with the markdown to PDF convert, graph above is not completed, but that should be fine, as long as you get the idea)

The submitted version is a little bit more complex than this, but the idea is the same.

I tested this program using 12 files, with 4 files have ~1KB size, and 8 files have ~6.2MB, and run each strategy 5 times on my pc and got the following:

|             | singleThread | threadPool4Topology | threadPool8Topology |
| ----------- | ------------ | ------------------- | ------------------- |
| Avg Runtime | 10.46 s      | 3.42 s              | 3.44 s              |

4 threads achieve 3 times faster while 8 threads have similar behavior. It maybe that my 4-core machine is not that good at running 8 threads.

And 4 threads did not achieve 4 times faster may be because that this problem is not a very typical embarrassingly parallel task and there are overhead caused by synchronization.

## About this Project

There are many classes not introduced in this document, please see the sources code if you're interested. Below is an UML graph.

#### UML

<img src="00.png">

<img src="01.png">

<img src="03.png">

(generated using online tools <http://www.plantuml.com/plantuml/uml/>)

(All classes in the above graphs are simplified)