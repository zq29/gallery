#ifndef ZQ_LOG
#define ZQ_LOG

#include <iostream>
#include <sstream>
#include <thread>
#include <mutex>
#include <atomic>

namespace zq {
namespace zqInner {
	using namespace std;
	
	/*
	* an interface helps to print log
	*/
	class Loggable {
	public:
		virtual string getClassName() const = 0;
		virtual string toStr() const = 0;
	};
	
	/*
	* an utility class, with all helper methods being static
	* no need to instantiate this class 
	*/
	class Log {
	private:
		/*
		* helper functions for <msg>
		*/
		static void _msg(stringstream& in) {}
		template<typename T, typename... Rest>
		static void _msg(stringstream& in, const T& t, const Rest&... args) {
			in << t;
			_msg(in, args...);
		}
		
		/*
		* make it thread safe
		*/
		static mutex printLock;
		
		/*
		* control what to show
		*/
		static atomic<bool> noVerbose; // default <true>
		static atomic<bool> noInfo; // default <false>
		static atomic<bool> noWarning; // default <false>
		static atomic<bool> noError; // default <fasle>
	public:
		/*
		* construct a string from concatenating <args>
		*/
		template<typename... Args>
		static string msg(Args... args) {
			stringstream ss;
			_msg(ss, args...);
			return ss.str();
		}
		/*
		* restriction:
		* 	1. <Seq> supports range based iteration
		* 	2. element of <Seq> supports <<
		*/
		template<typename Seq>
		static string seq2str(const Seq& seq) {
			stringstream ss;
			for(auto const& e : seq) {
				ss << e << " ";
			}
			return ss.str();
		}
		
		static void setVerbose(const bool b) {
			noVerbose = !b;
		}
		static void setInfo(const bool b) {
			noInfo = !b;
		}
		static void setWarning(const bool b) {
			noWarning = !b;
		}
		static void setError(const bool b) {
			noError = !b;
		}
		
		static void verbose(const string& msg) {
			if(noVerbose) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**VERBOSE** " << msg << endl;
		}
		static void verbose(const Loggable& obj, const string& msg) {
			if(noVerbose) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**VERBOSE** " << obj.getClassName() << ": " << msg << endl;
		}
		static void info(const string& msg) {
			if(noInfo) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**INFO** " << msg << endl;
		}
		static void info(const Loggable& obj, const string& msg) {
			if(noInfo) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**INFO** " << obj.getClassName() << ": " << msg << endl;
		}
		static void warning(const string& msg) {
			if(noWarning) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**WARNING** " << msg << endl;
		}
		static void warning(const Loggable& obj, const string& msg) {
			if(noWarning) { return; }
			unique_lock<mutex> lk(printLock);
			cout << "**WARNING** " << obj.getClassName() << ": " << msg << endl;
		}
		static void error(const string& msg) {
			if(noError) { return; }
			unique_lock<mutex> lk(printLock);
			cerr << "**ERROR** " << msg << endl;
		}
		static void error(const Loggable& obj, const string& msg) {
			if(noError) { return; }
			unique_lock<mutex> lk(printLock);
			cerr << "**ERROR** " << obj.getClassName() << ": " << msg << endl;
		}
	};
	
	mutex Log::printLock;
	atomic<bool> Log::noVerbose(true);
	atomic<bool> Log::noInfo(false);
	atomic<bool> Log::noWarning(false);
	atomic<bool> Log::noError(false);
}
	using zqInner::Loggable;
	using zqInner::Log;
}

#endif
