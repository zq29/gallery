#ifndef ZQ_THREADPOOL
#define ZQ_THREADPOOL

#include <vector>
#include <queue>
#include <stdexcept>
#include <thread>
#include <mutex>
#include <atomic>
#include <condition_variable>

#include "any.hpp"
#include "log.hpp"
#include "task.hpp"

namespace zq {
namespace zqInner {
	using namespace std;
	
	/*
	* TODO: write a intro for every class!
	*/
	class ThreadPool : public virtual Loggable {
	private:
		const unsigned nThreads;
		
		mutex taskQueueLock;
		condition_variable taskQueueCV;
		queue<function<void(void)>> taskQueue;
		
		/*
		* use a lock instead of atomic is because that 
		* condition variable will use a lock anyway
		*/
		mutex nRunningLock;
		condition_variable nRunningCV;
		unsigned nRunning;
		
		vector<thread> workerThreads;
		bool stop;
		/*
		*
		*/
		void runWorker() {
			while(!stop) {
				function<void(void)> f;
				// wait for task
				{
				unique_lock<mutex> lock(taskQueueLock);
				taskQueueCV.wait(lock, [this](){
					return taskQueue.size() > 0;
				});
				// run task
				f = taskQueue.front(); // it's a ref, use it before pop()!
				taskQueue.pop();
				}
				f();
				// signal done
				{ // auto release unique lock with scope
				unique_lock<mutex> lock2(nRunningLock);
				nRunning--;
				assert(nRunning >= 0);
				}
				nRunningCV.notify_one();
				Log::verbose(*this, "in function <runWorkder>, a task was done!");
			}
		}
	public:
		ThreadPool(unsigned n = 4) : nThreads(n), stop(false), nRunning(0) {
			if(n < 1) {
				const string msg = "in constructor, require nThread > 0";
				Log::error(*this, msg);
				throw invalid_argument(msg);
			}
			for(unsigned i = 0; i < nThreads; i++) {
				workerThreads.push_back(thread(
					[this](){runWorker();}
				));
			}
			Log::info(*this, Log::msg(nThreads, " threads created!"));
		}
		
		// non-copyable
		ThreadPool(const ThreadPool& rhs) = delete;
		ThreadPool(ThreadPool&& rhs) = delete;
		ThreadPool& operator=(const ThreadPool& rhs) = delete;
		ThreadPool& operator=(ThreadPool&& rhs) = delete;
		
		void feed(const function<void(void)>& f, bool dummyCall = false) {
			if(stop && !dummyCall) {
				const string msg = "in function <feed>, you've already invoked <finish>, this thread pool is no longer useable!";
				Log::error(*this, msg);
				throw logic_error(msg);
			}
			{
			unique_lock<mutex> lock(nRunningLock);
			nRunning++;
			}
			{
			unique_lock<mutex> lock(taskQueueLock);
			taskQueue.push(f);
			}
			taskQueueCV.notify_one();
		}
		
		/*
		* BLOCK!
		*/
		void waitAll() {
			if(stop) {
				Log::warning(*this, "in function <waitAll>, you've already invoked <finish>, this thread pool is no longer useable!");
			}
			unique_lock<mutex> lock(nRunningLock);
			nRunningCV.wait(lock, [this](){
				return nRunning == 0;
			});
			assert(taskQueue.size() == 0);
			Log::info(*this, "in function <waitALL>, all current works are done, continue!");
		}
		
		/*
		* BLOCK!
		*/
		void finish() {
			if(stop) {
				Log::warning(*this, "in function <finish>, you've already invoked <finish>, this thread pool is no longer useable!");
			}		
			/*
			* this means the threadpool won't be considered as finished
			* until all tasks in queue are completed, rather than
			* currently running tasks are completed
			*/
			waitAll();
			stop = true;
			// tell threads to execute a dummy task and exit
			auto dummyTask = [](){};
			for(unsigned i = 0; i < nThreads; i++) {
				feed(dummyTask, true);
			}
			taskQueueCV.notify_all();
			
			for(auto& worker : workerThreads) {
				worker.join();
				Log::info(*this, "in function <finish>, a thread exits!");
			}
		}
		
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "ThreadPool";
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};
}
	using zqInner::ThreadPool;
}

#endif
