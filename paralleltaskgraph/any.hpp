// idea from boost.any

#ifndef ZQ_ANY
#define ZQ_ANY

namespace zq {
namespace zqInner {
	using namespace std;

	class Any {
	private:
		shared_ptr<void> dataPtr; // performs type-erasure
	public:
		template<typename T>
		Any(T&& d = T()) : dataPtr(make_shared<T>(d)) {}
		
		template<typename T>
		shared_ptr<T> cast() const {
			return static_pointer_cast<T>(dataPtr);
		}
		
		template<typename T>
		const T& get() const {
			return *(cast<T>());
		}
	};
}
	using zqInner::Any;
}

#endif
