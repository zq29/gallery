#ifndef ZQ_STRATEGY
#define ZQ_STRATEGY

#include "log.hpp"
#include "threadpool.hpp"

namespace zq {
namespace zqInner {
	using namespace std;
	
	/*
	* forward declaration so to avoid circular include
	* use a pointer of <TaskDAG> so to set a fixed size 
	*/
	template<typename T>
	class TaskDAG;
	
	template<typename T> // for <TaskDAG<T>>
	class TaskDAGStrategy : public virtual Loggable {
	public:
		virtual ~TaskDAGStrategy() {}
		/*
		* return value is held by TaskDAG, so it's safe to return a reference
		*/
		virtual const T& run(zq::zqInner::TaskDAG<T>* g, int id) = 0;
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "TaskDAGStrategy";
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};
	
	template<typename T>
	class SingleThreadStrat final : public TaskDAGStrategy<T>, public virtual Loggable {
	public:
		virtual ~SingleThreadStrat() {};
		virtual const T& run(TaskDAG<T>* g, int id) override {
			return g->runOperation(id);
		}
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "SingleThreadStrat";
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};
	
	template<typename T, int nThreads = 4>
	class TopologyThreadPoolStrat final : public TaskDAGStrategy<T>, public virtual Loggable {
	private:
		ThreadPool tp;
	public:
		TopologyThreadPoolStrat() : tp(nThreads) {}
		virtual ~TopologyThreadPoolStrat() {
			tp.finish();
		}
		// TODO: only run id and its dependencies
		virtual const T& run(TaskDAG<T>* g, int id) override {
			vector<vector<int>> taskBatch = g->layeredTopologicalSort();
			// run each batch
			for(auto const& batch : taskBatch) {
				for(int i : batch) {
					Log::verbose(*this, Log::msg("in function <run>, feed ", i));
					/*
					* this took me more than 4 hours to debug:
					* at first, everything was by reference "[&]",
					* so <i> was caputured by ref, after <feed>, <i> increased,
					* when the operation actually ran (in another thread),
					* <i> had already been changed !!!
					* so use "[&g, i]" instead!
					*/
					tp.feed([&g, i]() {g->runSingleOperation(i);});
				}
				tp.waitAll();
			}
			//tp.finish();
			return g->getResult(id);
		}
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return Log::msg("TopologyThreadPool", nThreads, "Strat");
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};
}
	using zqInner::TaskDAGStrategy;
	using zqInner::SingleThreadStrat;
	using zqInner::TopologyThreadPoolStrat;
}

#endif
