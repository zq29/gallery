#ifndef ZQ_GRAPH
#define ZQ_GRAPH

#include <iostream>
#include <sstream>
#include <unordered_map>
#include <list>
#include <queue>
#include <algorithm>
#include <stdexcept>
#include <assert.h>

#include "log.hpp"

namespace zq {
namespace zqInner {
	using namespace std;
	/*
	* assumptions about <NodeDType>:
	* 	1. move copy constructor is defined
	* 	2. copy constructor is defined
	*/
	template<typename NodeDType> // node data type
	class DirectedGraph : public virtual Loggable {
	private:
		/*
		* a wrapper for a node
		* add <inDegree> to a node
		*/
		class Node {
		public:
			NodeDType data;
			unsigned inDegree; // in degree, could be used for topological sort
			Node(NodeDType&& d, unsigned i = 0) :
				data(d), inDegree(i) {}
			Node() : inDegree(0) {}
		};	
	protected:
		/*
		* <nodeId, Node>, stores the data
		*/
		unordered_map<int, Node> nodes;
		/*
		* assumpution: list size is small
		* <nodeId, list of adj nodes>, stores all edges
		*/
		unordered_map<int, list<int>> edges;
		/*
		* <currNodeId, list of innodes ids>
		* store ids of nodes who point at <currNodeId>
		*/
		unordered_map<int, list<int>> inNodes;
	public:
		/*
		* return a node id, which can be later used to index this node
		*/
		int addNode(NodeDType&& nodeData) {
			const int id = nodes.size();
			nodes.insert(make_pair(id, Node(move(nodeData), 0)));
			return id;	
		}
		/*
		* add a DIRECTED edge from <id1> to <id2>
		* before add:
		* 	1. check if nodes with <id1> and <id2> exist, if not, throw exception
		* 	2. check if the edge is already there, add edge only if not
		*/
		virtual void addEdge(int id1, int id2) {
			auto t1 = nodes.find(id1);
			auto t2 = nodes.find(id2);
			if(t1 == nodes.end() || t2 == nodes.end()) {
				const string msg = Log::msg(
					"in function <addEdge>, node ",
					(t1 == nodes.end() ? id1 : id2),
					" does not exist!"
				);
				Log::error(*this, msg);
				throw invalid_argument(msg);
			}
			
			auto& tList = edges[id1];
			if(find(tList.begin(), tList.end(), id2) == tList.end()) {
				// actually add
				edges[id1].push_back(id2);
				nodes[id2].inDegree++;
				inNodes[id2].push_back(id1);
			}
		}
		
		/*
		* get the non-const REFERENCE of a node
		*/
		NodeDType& getNodeRef(int id) {
			if(nodes.find(id) == nodes.end()) {
				const string msg = Log::msg("in function <getNode>, no node has id ", id);
				Log::error(*this, msg);
				throw runtime_error(msg);
			}
			return nodes[id].data;
		}
		
		/*
		* get a copy of a node
		*/
		NodeDType getNodeCopy(int id) {
			if(nodes.find(id) == nodes.end()) {
				const string msg = Log::msg("in function <getNode>, no node has id ", id);
				Log::error(*this, msg);
				throw runtime_error(msg);
			}
			return nodes[id].data;
		}
		
		/*
		* get all ids of its adjacencies of a node given its id 
		*/
		const list<int>& getAdjs(int id) {
			if(nodes.find(id) == nodes.end()) {
				const string msg = Log::msg("in function <getAdjs>, id ", id, " does not exist!");
				Log::error(msg);
				throw runtime_error(msg);
			}
			return edges[id];
		}
		
		/*
		* get all ids of its innodes
		*/
		const list<int>& getInnodes(int id) {
			if(nodes.find(id) == nodes.end()) {
				const string msg = Log::msg("in function <getInnodes>, id ", id, " does not exist!");
				Log::error(msg);
				throw runtime_error(msg);
			}
			return inNodes[id];
		}
		
		unsigned getIndegree(int id) {
			if(nodes.find(id) == nodes.end()) {
				const string msg = Log::msg("in function <getIndegree>, id ", id, " does not exist!");
				Log::error(msg);
				throw runtime_error(msg);
			}
			return inNodes[id].size();
		}
		
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "DirectedGraph";
		}
		virtual string toStr() const override {
			stringstream ss;
			for(auto const& e : edges) {
				ss << "node " << e.first << " points to ";
				for(auto neighbor : e.second) {
					ss << neighbor << " ";
				}
			}
			return ss.str();
		}
	};

	/*
	* DAG class: a directd graph without directed circle
	*/
	template<typename NodeDType>
	class DirectedAcyclicGraph : 
		public DirectedGraph<NodeDType>, public virtual Loggable {
	private:
		/*
		* do topological sort, return false if found circle in graph
		* store sorted node ids in <res>
		*/
		bool _topologicalSort(vector<int>& res) {
			assert(res.size() == 0);
			// init
			unordered_map<int, unsigned> _nodes; // <id, inDegree>, no need to know the data
			queue<int> zeros; // ids of nodes whose in-degrees are 0
			for(auto const& e : this->nodes) {
				_nodes[e.first] = e.second.inDegree;
				if(e.second.inDegree == 0) {
					zeros.push(e.first);
				}
			}
			
			// begin!
			while(!zeros.empty()) {
				const int curr = zeros.front();
				res.push_back(curr);
				zeros.pop();
				// update
				for(int neighbor : this->getAdjs(curr)) {
					assert(_nodes[neighbor] > 0);
					_nodes[neighbor]--;
					if(_nodes[neighbor] == 0) {
						zeros.push(neighbor);
					}
				}
			}
			
			// check result
			for(auto const& e : _nodes) {
				if(e.second != 0) { // circle detected!
					return false;
				}
			}
			return true;
		}
	
	public:
		/*
		* use topological sort to check if there are circles
		*/
		bool hasCircle() {
			vector<int> res;
			return !_topologicalSort(res);
		}
		/*
		* get the sorted node ids
		* if circles exist, throw an exception
		*/
		vector<int> topologicalSort() {
			vector<int> res;
			if(!_topologicalSort(res)) {
				const string msg = "in function <topologicalSort>, circle detected in DAG!";
				Log::error(*this, msg);
				throw logic_error(msg);	
			}
			return res;
		}
		
		/*
		* 
		*/
		vector<vector<int>> layeredTopologicalSort() {
			// init
			vector<vector<int>> res;
			unordered_map<int, unsigned> _nodes; // <id, inDegree>, no need to know the data
			list<int> zeros; // ids of nodes whose in-degrees are 0
			for(auto const& e : this->nodes) {
				_nodes[e.first] = e.second.inDegree;
				if(e.second.inDegree == 0) {
					zeros.push_back(e.first);
				}
			}
			
			// begin!
			while(!zeros.empty()) {
				res.push_back(vector<int>(zeros.begin(), zeros.end())); 
				
				list<int> buffer;
				while(!zeros.empty()) {
					const int curr = zeros.front();
					zeros.erase(zeros.begin());
					// update
					for(int neighbor : this->getAdjs(curr)) {
						assert(_nodes[neighbor] > 0);
						_nodes[neighbor]--;
						if(_nodes[neighbor] == 0) {
							buffer.push_back(neighbor);
						}
					}
				}
				
				zeros = buffer;
			}
			
			// check result
			for(auto const& e : _nodes) {
				if(e.second != 0) { // circle detected!
					const string msg = "in function <layeredTopologicalSort>, "\
						"circle has been detected, you should not invoke this method "\
						"before making sure the DAG has no circle in it!";
					Log::error(msg);
					throw runtime_error(msg);
				}
			}
			return res;
		}
		
		virtual void addEdge(int id1, int id2) override {
			DirectedGraph<NodeDType>::addEdge(id1, id2);
			if(hasCircle()) {
				const string msg = Log::msg(
					"in function <addEdge>, after adding edge ",
					id1, "->", id2,
					", a circle is detected in DAG, abort!");
				Log::error(*this, msg);
				throw logic_error(msg);
			}
		}
	};
	
	template<typename NodeDType>
	using DAG = DirectedAcyclicGraph<NodeDType>; // alias
}
	using zqInner::DirectedGraph;
	using zqInner::DirectedAcyclicGraph;
	using zqInner::DAG;
}

#endif
