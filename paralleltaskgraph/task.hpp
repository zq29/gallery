#ifndef ZQ_TASK
#define ZQ_TASK

#include <functional>
#include <tuple>
#include <typeinfo>

#include "log.hpp"

namespace zq {
namespace zqInner {
	using namespace std;
	
	/*
	* A Task
	* 1. it can be a function wrapper, which takes an argument vector
	* 	of type <T> as input and return a result of type <T>. What is
	* 	wanting multiple types (of arguments or return type)? Use <Any>
	* 	class (TODO) which hides type info, but you need to EXPLICITLY
	* 	do a type cast in the function <f> you passed in
	* 2. it can be a placeholder, which does no calculation but holds a value,
	* 	its in-degree is 0 (a leaf node if you're using a task tree)
	* ATTENTION: T cannot be <void>, since you're building a data flow, void doesn't make sense
	* 	as for top level node, return a dummy one if you want to ignore the result 
	*/
	template<typename T>
	class Task : public virtual Loggable {
	private:
		function<T(vector<T>)> f; // vector<source IDs>
		bool resValid; // indicates if result is valid
		shared_ptr<T> res; // maybe referenced in other places
		const bool _isPlaceholder; // a placeholder is a node with in-degree 0 and no <f> defined
		
	public:
		/*
		* use constructor below to specify node type (is placeholder or not)
		* and also, delete this for conducting type checking
		*/
		Task() = delete;
		/*
		* get a placeholder
		*/
		Task(const T& r) : resValid(true), res(make_shared<T>(r)), _isPlaceholder(true) {
			// <T&> guarantees that T cannot be void
		}	
		/*
		* get a non-placeholder
		*/
		Task(const function<T(vector<T>)>& _f) : 
			f(_f), resValid(false), _isPlaceholder(false) {
			// vector<T> guarantees that T cannot be <void>
		}
		/*
		*
		*/
		bool isPlaceholder() const {
			return _isPlaceholder;
		}
		/*
		* <resValid> helps avoid repetitive calculation 
		*/
		bool isResValid() const {
			return resValid;
		}
		/*
		* simple setter
		*/
		void setValid(bool v) {
			resValid = v;
		}
		
		shared_ptr<const T> getResPtr() const {
			if(!resValid) {
				Log::warning(*this, "in function <getRes>, got an invalid result!");
			}
			if(!res) {
				Log::warning(*this, "in function <getRes>, got an nullptr, which may cause segment fault!");
			}
			return res;
		}
		
		/*
		* <shared_ptr<const T> getResPtr() const> is suggested, since
		* this method may introduce overhead from copying an obj
		* and it raises exception
		*/
		T getResult() const {
			if(!resValid) {
				Log::warning(*this, "in function <getRes>, got an invalid result!");
			}
			if(!res) {
				const string msg = "in function <getRes>, tried to de-reference from null!";
				Log::error(*this, msg);
				throw runtime_error(msg);
			}
			return *res;
		}
		
		/*
		* skip optimization, run <f> with args
		*/
		void forceRun(const vector<T>& args) {
			if(_isPlaceholder) { return; } // nothing to run
			res = make_shared<T>(f(args));
			resValid = true;
		}
		/*
		* if result is still valid, skip
		*/
		void run(const vector<T>& args) {
			if(_isPlaceholder) { return; }
			if(resValid) { 
				Log::verbose(*this, "in function <run>, yay! repetitive running eliminated!");
				return; 
			}
			forceRun(args);
		}
		/*
		* use this ONLY to change a placeholder's value
		* TODO: invalid the whole path
		*/
		void setRes(shared_ptr<T> r) {
			if(!_isPlaceholder) {
				Log::warning(*this, "in function <setRes>, set value to a non-placeholder!");
			}
			res = r;
			resValid = true;
		}
		void setValue(const T& r) { 
			setRes(make_shared<T>(r));
		}
		
		/*
		* implement <Loggable> interface
		*/
		virtual string getClassName() const override {
			return "Task";
		}
		virtual string toStr() const override {
			return "TODO: o~";
		}
	};
}
	using zqInner::Task;
}

#endif
