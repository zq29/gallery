#include <iostream>
#include <vector>
#include <memory>
#include <cstdlib>
#include <fstream>
#include <stdexcept>
#include <unordered_set>
#include <assert.h>
#include <string.h>
#include <cmath>

#include "log.hpp"
#include "any.hpp"
#include "graph.hpp"
#include "taskdag.hpp"
#include "test/testGraph.hpp"
#include "test/testTaskDAG.hpp"
#include "test/testAny.hpp"
#include "test/testThreadPool.hpp"

using namespace zq;
using namespace std;

void runTests();
void runTFIDF(const vector<string>& args, TaskDAG<Any>::Strategy s);

int main(int argc, char** argv) {
	/*
	Log::setVerbose(true);
	runTests();
	*/
	
	vector<string> args = {
		"./data/dict.txt", 
		"./data/1.txt", "./data/2.txt", "./data/3.txt", "./data/4.txt",
		"./data/big1.txt", "./data/big2.txt", "./data/big3.txt",
		"./data/big4.txt", "./data/big5.txt", "./data/big6.txt",
		"./data/big7.txt", "./data/big8.txt"
		};
		
	if(argc == 1) {
		runTFIDF(args, TaskDAG<Any>::Strategy::s3);
	} else if(argc == 2) {
		if(strcmp(argv[1], "s1") == 0) {
			runTFIDF(args, TaskDAG<Any>::Strategy::s1); 
		} else if(strcmp(argv[1], "s2") == 0) {
			runTFIDF(args, TaskDAG<Any>::Strategy::s2); 
		} else if(strcmp(argv[1], "s3") == 0) {
			runTFIDF(args, TaskDAG<Any>::Strategy::s3); 
		} else {
			Log::error("Usage: \"./main [strategy]\" with strategy being <s1> or <s2> or <s3> or null");
			return EXIT_FAILURE;
		}
	} else {
		Log::error("Usage: \"./main [strategy]\" with strategy being <s1> or <s2> or <s3> or null");
		return EXIT_FAILURE;
	}
		
	return EXIT_SUCCESS;	
}

// see a TF-IDF example http://www.tfidf.com/
// dict file from https://github.com/first20hours/google-10000-english
// big file from https://norvig.com/big.txt
void runTFIDF(const vector<string>& args, TaskDAG<Any>::Strategy s) {
	TaskDAG<Any> g;
	g.setStrategy(s);
	
	// place holder: 1 dict & at least 2 text files
	int dictFilenameId = g.addPlaceholder(string(args[0]));
	vector<int> filenameIds;
	for(int i = 1; i < args.size(); i++) {
		filenameIds.push_back(g.addPlaceholder(string(args[i])));
	}

	// get dict	
	auto getDict = [](const vector<Any>& args) {
		Log::info("Getting dict...");
		assert(args.size() == 1);
		const string filename = args[0].get<string>();
		ifstream f(filename);
		if(!f) {
			const string msg = Log::msg("Failed to open file ", filename);
			Log::error(msg);
			throw runtime_error(msg);
		}
		// go!
		string word;
		set<string> dict;
		while(f >> word) {
			dict.insert(word);
		}
		//Log::verbose(Log::msg("Got dict: ", Log::seq2str(dict)));
		return Any(move(dict));
	};
	int dictId = g.addOperation(getDict, {dictFilenameId});
	
	// vectorize file
	auto getFileVec = [](const vector<Any>& args)->Any {
		assert(args.size() == 2);
		const string filename = args[0].get<string>();
		Log::info(Log::msg("Vectorizing file ", filename, " ..."));
		auto const& dict = args[1].get<set<string>>();
		ifstream f(filename);
		if(!f) {
			const string msg = Log::msg("Failed to open file ", filename);
			Log::error(msg);
			throw runtime_error(msg);
		}
		// go!
		string word;
		unordered_map<string, unsigned> wordCount;
		while(f >> word) {
			if(dict.find(word) == dict.end()) { continue; }
			if(wordCount.find(word) == wordCount.end()) {
				wordCount[word] = 1;
			} else {
				wordCount[word]++;
			}
		}
		/*
		for(auto const& p : wordCount) {
			Log::verbose(Log::msg("word count: ", p.first, " ", p.second));
		}
		*/
		return Any(move(wordCount));
	};
	vector<int> wordCountIds;
	for(int e : filenameIds) {
		wordCountIds.push_back(g.addOperation(getFileVec, {e, dictId}));
	}
	
	// get TF
	auto getTF = [](const vector<Any>& args) {
		Log::info("Calculating TF for a file...");
		assert(args.size() == 1);
		auto const& wordCount = args[0].get<unordered_map<string, unsigned>>();
		unordered_map<string, double> TF;
		unsigned totalWord = 0;
		for(auto const& p : wordCount) {
			totalWord += p.second;
		}
		for(auto const& p : wordCount) {
			TF[p.first] = 1.0 * p.second / totalWord;
		}
		return Any(move(TF));
	};
	vector<int> TFIds;
	for(const int i : wordCountIds) {
		TFIds.push_back(g.addOperation(getTF, {i}));
	}
	
	// get IDF
	auto getIDF = [](const vector<Any>& args) {
		Log::info("Calculating IDF...");
		assert(args.size() > 1);
		unordered_map<string, double> IDF;
		for(unsigned i = 0; i < args.size(); i++) {
			auto const& wordCount = args[i].get<unordered_map<string, unsigned>>();
			for(auto const& p : wordCount) {
				assert(p.second != 0);
				if(IDF.find(p.first) == IDF.end()) {
					IDF[p.first] = 1;
				} else {
					IDF[p.first] += 1;
				}
			} // for every word
		} // for every file (wordCount)
		
		// transform
		const unsigned nFile = args.size();
		for(auto& p : IDF) {
			/*
			* if prev IDF[word] is 0, it means this word does not exist in any file,
			* so the value of IDF does not matter as long as it is finite,
			* since TF is 0 for this word in any file
			*/
			assert(p.second != 0);
			p.second = (log2(1.0 * nFile / p.second));
			//Log::verbose(Log::msg("IDF: ", p.first, " ", p.second));
		}
		
		return Any(move(IDF));
	};
	int IDFId = g.addOperation(getIDF, wordCountIds);
	
	// get TF-IDF
	auto getTFIDF = [](const vector<Any>& args) {
		Log::info("Calculating TF-IDF for a file...");
		assert(args.size() == 3);
		auto const& dict = args[0].get<set<string>>();
		auto const& TF = args[1].get<unordered_map<string, double>>();
		auto const& IDF = args[2].get<unordered_map<string, double>>();
		unordered_map<string, double> _TFIDF;
		for(auto const& tp : TF) {
			assert(IDF.find(tp.first) != IDF.end());
			_TFIDF[tp.first] = tp.second * IDF.at(tp.first);
		}
		// re-oreder, to sparse vector
		vector<double> TFIDF(dict.size());
		for(const string& word : dict) {
			TFIDF.push_back(_TFIDF[word]);
		}
		return Any(move(TFIDF));
	};
	vector<int> TFIDFIds;
	for(const int i : TFIds) {
		TFIDFIds.push_back(g.addOperation(getTFIDF, {dictId, i, IDFId}));
	}
	
	/*
	// below is the correct executing order
	for(int i : filenameIds) cout << i << " ";
	cout << dictFilenameId << endl;
	cout << dictId << endl;
	for(int i : wordCountIds) cout << i << " ";
	cout << endl;
	for(int i : TFIds) cout << i << " ";
	cout << IDFId << endl;
	for(int i : TFIDFIds) cout << i << " ";
	cout << endl << endl;
	*/
	
	for(const int i : TFIDFIds) {
		g.run(i);
	}
	
	// TODO: sub-graph
	
	// ------------------ RUN SERIAL ----------------------------
	// cosine similarity 
	auto getSim = [](const vector<double>& a, const vector<double>& b) {
		assert(a.size() == b.size());
		double aLen = 0, bLen = 0, dotProd = 0;
		for(int i = 0; i < a.size(); i++) {
			dotProd += a[i] * b[i];
			aLen += a[i] * a[i];
			bLen += b[i] * b[i];
		}
		aLen = sqrt(aLen);
		bLen = sqrt(bLen);
		double sim = dotProd / (aLen * bLen);
		assert(sim > -1e-5 && sim < 1 + 1e-5);
		return sim;
	};
	
	assert(TFIDFIds.size() == filenameIds.size());
	//<similiaity, first file index, second file index>
	vector<tuple<double, unsigned, unsigned>> sims;
	for(unsigned i = 0; i < TFIDFIds.size(); i++) {
		for(unsigned j = i + 1; j < TFIDFIds.size(); j++) {
			double sim = getSim(
				g.getResult(TFIDFIds[i]).get<vector<double>>(), 
				g.getResult(TFIDFIds[j]).get<vector<double>>()
			);
			sims.push_back(make_tuple(sim, i, j));
		}
	}
	sort(sims.begin(), sims.end());
	for(auto const& t : sims) {
		Log::info(Log::msg(
			g.getResult(filenameIds[get<1>(t)]).get<string>(),
			" and ",
			g.getResult(filenameIds[get<2>(t)]).get<string>(),
			" has similarity ",
			get<0>(t)
		));
	}
}

void runTests() {
	cout << "---------------------- TEST GRAPH -----------------------" << endl;
	zqTestGraph::testGraph();
	cout << "\n\n\n\n\n\n\n\n\n\n\n";
	cout << "---------------------- TEST TASKDAG -----------------------" << endl;
	zqTestTaskDAG::testTaskDAG();
	cout << "\n\n\n\n\n\n\n\n\n\n\n";
	cout << "---------------------- TEST ANY -----------------------" << endl;
	zqTestAny::testAny();
	cout << "\n\n\n\n\n\n\n\n\n\n\n";
	cout << "---------------------- TEST THREADPOOL -----------------------" << endl;
	zqTestThreadPool::testThreadPool();
	cout << "\n\n\n\n\n\n\n\n\n\n\n";
}
