# Gallery

A gallery of my projects!

Every projects has a README in it, check them for more details!

## Highlights

#### Parallel Task Graph

[Check here for more details about this project!](./paralleltaskgraph)

You tell the graph of data dependencies, let it automatically run parallelly and handle synchronizations for you! You can run the following TF-IDF thing **3x faster** than a single threaded program **without writing multi-thread code!** (let the framework handle it for you)

Written in C++ with

* Virtual inheritance
* Template over graph definition
* C++ 11 features including lambda, shared_ptr
* RAII like lock_guard
* My own threadpool implementation
* A thread-safe log

```mermaid
graph TD
l01((dict.txt))
l02((text1.txt))
l03((text2.txt))
l04((text3.txt))

l11(dictFileName)
l12(filename1)
l13(filename2)
l14(filename3)

l01-->l11
l02-->l12
l03-->l13
l04-->l14

l21(readDict)

l31(vectorizeFile1)
l32(vectorizeFile2)
l33(vectorizeFile3)

l11-->l21
l12-->l31
l13-->l32
l14-->l33
l21-->l31
l21-->l32
l21-->l33

l41(getIDF)
l42(getTF1)
l43(getTF2)
l44(getTF3)

l31-->l41
l32-->l41
l33-->l41
l31-->l42
l32-->l43
l33-->l44

l51(getTF-IDF1)
l52(getTF-IDF2)
l53(getTF-IDF3)
l42-->l51
l43-->l52
l44-->l53
l41-->l51
l41-->l52
l41-->l53

l61(getSimilarity1)
l62(getSimilarity2)
l51-->l61
l52-->l61
l52-->l62
l53-->l62
```



## About Me

You might find this repo from my 

* LinkedIn: [https://www.linkedin.com/in/zuncheng-qian/](http://www.qianzuncheng.com/en)
* My Website: [http://www.qianzuncheng.com/en](http://www.qianzuncheng.com/en)

If that's not the case, check the links above to find out more about me!